package com.play.utils;

import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Properties;

@Component
public class ServerProperties {
    private Properties properties = new Properties();
    private static final String SERVER_PROPERTIES_FILE_NAME = "server.properties";
    private static final String SERVER_LOCAL_PROPERTIES_FILE_NAME = "server_local.properties";

    public ServerProperties() {
        try {
            InputStream serverProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(SERVER_PROPERTIES_FILE_NAME);
            properties.load(serverProperties);
            serverProperties.close();
            InputStream serverLocalProperties = Thread.currentThread().getContextClassLoader().getResourceAsStream(SERVER_LOCAL_PROPERTIES_FILE_NAME);
            properties.load(serverLocalProperties);
            serverLocalProperties.close();
        }
        catch(Exception excpetion) {
            System.out.println("Exception!");
        }
    }

    public String getProperty(String propertyName) {
        return properties.getProperty(propertyName);
    }
}
