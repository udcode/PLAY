package com.play.utils;

import com.play.data.BaseEnum;
import com.play.data.Pair;

import java.util.HashMap;

public class EnumUtil {
    private static EnumUtil enumUtilInstance = null;
    private HashMap<Pair<Class, Integer>, BaseEnum> numberValuesToEnums = new HashMap<Pair<Class, Integer>, BaseEnum>();

    public static EnumUtil get(){
        if (enumUtilInstance == null) {
            enumUtilInstance = new EnumUtil();
        }

        return enumUtilInstance;
    }

    public void add(BaseEnum enumObject) {
        numberValuesToEnums.put(new Pair<Class, Integer>(enumObject.getClass(), enumObject.getNumberValue()), enumObject);
    }

    public BaseEnum getByClassAndNumberValue (Class enumClass, int numberValue) {
        return numberValuesToEnums.get(new Pair<Class, Integer>(enumClass, numberValue));
    }
}
