package com.play.view;

import com.play.data.InstructorData;
import com.play.data.UserData;

public class InstructorView extends InstructorData {
    private UserData user;

    public InstructorView(InstructorData instructor) {
        super(instructor);
    }

    public InstructorView(InstructorData instructor, UserData user) {
        super(instructor);
        this.user = user;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }
}
