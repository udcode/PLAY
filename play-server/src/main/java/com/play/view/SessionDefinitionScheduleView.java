package com.play.view;

import com.play.data.SessionDefinitionData;
import com.play.data.SessionDefinitionScheduleData;

public class SessionDefinitionScheduleView extends SessionDefinitionScheduleData {
    private SessionDefinitionData sessionDefinition;

    public SessionDefinitionScheduleView(SessionDefinitionScheduleData sessionDefinitionSchedule) {
        super(sessionDefinitionSchedule);
    }

    public SessionDefinitionScheduleView(SessionDefinitionScheduleData sessionDefinitionSchedule, SessionDefinitionData sessionDefinition) {
        super(sessionDefinitionSchedule);
        this.sessionDefinition = sessionDefinition;
    }

    public SessionDefinitionData getSessionDefinition() {
        return sessionDefinition;
    }

    public void setSessionDefinition(SessionDefinitionData sessionDefinition) {
        this.sessionDefinition = sessionDefinition;
    }
}
