package com.play.view;

import com.play.data.PlayerActivityLevelData;

public class PlayerActivityLevelView extends PlayerActivityLevelData {
    private PlayerView player;
    private ActivityView activity;

    public PlayerActivityLevelView(PlayerActivityLevelData playerActivityLevel) {
        super(playerActivityLevel);
    }

    public PlayerActivityLevelView (PlayerActivityLevelData playerActivityLevel, PlayerView player, ActivityView activity) {
        super(playerActivityLevel);
        this.player = player;
        this.activity = activity;
    }

    public PlayerView getPlayer() {
        return player;
    }

    public void setPlayer(PlayerView player) {
        this.player = player;
    }

    public ActivityView getActivity() {
        return activity;
    }

    public void setActivity(ActivityView activity) {
        this.activity = activity;
    }
}
