package com.play.view;

import com.play.data.PlaceActivityData;

public class PlaceActivityView extends PlaceActivityData {
    private PlaceView place;
    private ActivityView activity;

    public PlaceActivityView(PlaceActivityData placeActivityData) {
        super(placeActivityData);
    }

    public PlaceActivityView(PlaceActivityData placeActivityData, PlaceView place, ActivityView activity) {
        super(placeActivityData);
        this.place = place;
        this.activity = activity;
    }

    public PlaceView getPlace() {
        return place;
    }

    public void setPlace(PlaceView place) {
        this.place = place;
    }

    public ActivityView getActivity() {
        return activity;
    }

    public void setActivity(ActivityView activity) {
        this.activity = activity;
    }
}