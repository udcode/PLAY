package com.play.view;

import com.play.data.MembershipData;
import com.play.data.PlayerData;
import com.play.data.UserData;

public class PlayerView extends PlayerData {
    private UserData user;
    private MembershipData membership;

    public PlayerView(PlayerData player) {
        super(player);
    }

    public PlayerView(PlayerData player, UserData user, MembershipData membership) {
        super(player);
        this.user = user;
        this.membership = membership;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }

    public MembershipData getMembership() {
        return membership;
    }

    public void setMembership(MembershipData membership) {
        this.membership = membership;
    }
}
