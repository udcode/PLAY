package com.play.view;

import com.play.data.PlaceOwnerData;
import com.play.data.UserData;

public class PlaceOwnerView extends PlaceOwnerData {
    private UserData user;

    public PlaceOwnerView(PlaceOwnerData placeOwner) {
        super(placeOwner);
    }

    public PlaceOwnerView(PlaceOwnerData placeOwner, UserData user) {
        super(placeOwner);
        this.user = user;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }
}
