package com.play.view;

import com.play.data.UserData;
import com.play.data.UserRoleData;
import com.play.data.UserRoleEnum;

import java.util.Collection;
import java.util.stream.Collectors;

public class UserView extends UserData {
    private UserData referenceUser;
    private Collection<UserRoleData> roles;

    public UserView(UserData user) {
        super(user);
    }

    public UserView(UserData user, UserData referenceUser, Collection<UserRoleData> roles) {
        super(user);
        this.referenceUser = referenceUser;
        this.roles = roles;
    }

    public UserData getReferenceUser() {
        return referenceUser;
    }

    public void setReferenceUser(UserData referenceUser) {
        this.referenceUser = referenceUser;
    }

    public Collection<UserRoleData> getRoles() {
        return roles;
    }

    public void setRoles(Collection<UserRoleData> roles) {
        this.roles = roles;
    }

    public boolean isPlayer() {
        return roles != null && roles.stream().map(r -> r.getRole()).collect(Collectors.toList()).contains(UserRoleEnum.player);
    }

    public boolean isInstructor() {
        return roles != null && roles.stream().map(r -> r.getRole()).collect(Collectors.toList()).contains(UserRoleEnum.instructor);
    }

    public boolean isPlaceOwner() {
        return roles != null && roles.stream().map(r -> r.getRole()).collect(Collectors.toList()).contains(UserRoleEnum.placeOwner);
    }

    public boolean isStaffMember() {
        return roles != null && roles.stream().map(r -> r.getRole()).collect(Collectors.toList()).contains(UserRoleEnum.staffMember);
    }

    public boolean isAdministrator() {
        return roles != null && roles.stream().map(r -> r.getRole()).collect(Collectors.toList()).contains(UserRoleEnum.administrator);
    }
}
