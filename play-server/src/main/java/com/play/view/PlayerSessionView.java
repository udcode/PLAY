package com.play.view;

import com.play.data.PlayerSessionData;

public class PlayerSessionView extends PlayerSessionData {
    private PlayerView player;
    private SessionView session;

    public PlayerSessionView(PlayerSessionData playerSession) {
        super(playerSession);
    }

    public PlayerSessionView(PlayerSessionData playerSession, PlayerView player, SessionView session) {
        super(playerSession);
        this.player = player;
        this.session = session;
    }

    public PlayerView getPlayer() {
        return player;
    }

    public void setPlayer(PlayerView player) {
        this.player = player;
    }

    public SessionView getSession() {
        return session;
    }

    public void setSession(SessionView session) {
        this.session = session;
    }
}
