package com.play.view;

import com.play.data.*;

import java.util.Collection;

public class SessionDefinitionView extends SessionDefinitionData {
    private InstructorData instructor;
    private PlaceData place;
    private MembershipData membership;
    private ActivityData activity;
    private Collection<SessionDefinitionScheduleData> schedules;

    public SessionDefinitionView(SessionDefinitionData sessionDefinitionData) {
        super (sessionDefinitionData);
    }

    public SessionDefinitionView(SessionDefinitionData sessionDefinitionData, InstructorData instructor, PlaceData place,
                                 MembershipData membership, ActivityData activity, Collection<SessionDefinitionScheduleData> schedules) {
        super (sessionDefinitionData);
        this.instructor = instructor;
        this.place = place;
        this.membership = membership;
        this.activity = activity;
        this.schedules = schedules;
    }

    public InstructorData getInstructor() {
        return instructor;
    }

    public void setInstructor(InstructorData instructor) {
        this.instructor = instructor;
    }

    public PlaceData getPlace() {
        return place;
    }

    public void setPlace(PlaceData place) {
        this.place = place;
    }

    public MembershipData getMembership() {
        return membership;
    }

    public void setMembership(MembershipData membership) {
        this.membership = membership;
    }

    public ActivityData getActivity() {
        return activity;
    }

    public void setActivity(ActivityData activity) {
        this.activity = activity;
    }

    public Collection<SessionDefinitionScheduleData> getSchedules() {
        return schedules;
    }

    public void setSchedules(Collection<SessionDefinitionScheduleData> schedules) {
        this.schedules = schedules;
    }
}
