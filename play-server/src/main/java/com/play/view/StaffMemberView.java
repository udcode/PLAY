package com.play.view;

import com.play.data.StaffMemberData;
import com.play.data.UserData;

public class StaffMemberView extends StaffMemberData {
    private UserData user;

    public StaffMemberView(StaffMemberData staffMember) {
        super(staffMember);
    }

    public StaffMemberView(StaffMemberData staffMember, UserData user) {
        super(staffMember);
        this.user = user;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }
}
