package com.play.view;

import com.play.data.PlaceData;

public class PlaceView extends PlaceData {
    private PlaceOwnerView placeOwner;

    public PlaceView(PlaceData place) {
        super(place);
    }

    public PlaceView(PlaceData place, PlaceOwnerView placeOwner) {
        super(place);
        this.placeOwner = placeOwner;
    }

    public PlaceOwnerView getPlaceOwner() {
        return placeOwner;
    }

    public void setPlaceOwner(PlaceOwnerView placeOwner) {
        this.placeOwner = placeOwner;
    }
}
