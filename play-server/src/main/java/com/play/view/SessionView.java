package com.play.view;

import com.play.data.MembershipData;
import com.play.data.PlaceData;
import com.play.data.SessionData;
import com.play.data.SessionDefinitionData;

public class SessionView extends SessionData {
    private SessionDefinitionData sessionDefinition;
    private InstructorView instructor;
    private PlaceData place;
    private MembershipData membership;

    public SessionView(SessionData session) {
        super(session);
    }

    public SessionView (SessionData session, SessionDefinitionData sessionDefinition, InstructorView instructor,
                        PlaceData place, MembershipData membership) {
        super(session);
        this.sessionDefinition = sessionDefinition;
        this.instructor = instructor;
        this.place = place;
        this.membership = membership;
    }

    public SessionDefinitionData getSessionDefinition() {
        return sessionDefinition;
    }

    public void setSessionDefinition(SessionDefinitionData sessionDefinition) {
        this.sessionDefinition = sessionDefinition;
    }

    public InstructorView getInstructor() {
        return instructor;
    }

    public void setInstructor(InstructorView instructor) {
        this.instructor = instructor;
    }

    public PlaceData getPlace() {
        return place;
    }

    public void setPlace(PlaceData place) {
        this.place = place;
    }

    public MembershipData getMembership() {
        return membership;
    }

    public void setMembership(MembershipData membership) {
        this.membership = membership;
    }
}
