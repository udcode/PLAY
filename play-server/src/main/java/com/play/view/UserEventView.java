package com.play.view;

import com.play.data.UserData;
import com.play.data.UserEventData;

public class UserEventView extends UserEventData{
    private UserData user;
    private StaffMemberView staffMember;

    public UserEventView(UserEventData userEvent) {
        super(userEvent);
    }

    public UserEventView(UserEventData userEvent, UserData user, StaffMemberView staffMember) {
        super(userEvent);
        this.user = user;
        this.staffMember = staffMember;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }

    public StaffMemberView getStaffMember() {
        return staffMember;
    }

    public void setStaffMember(StaffMemberView staffMember) {
        this.staffMember = staffMember;
    }
}
