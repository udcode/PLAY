package com.play.view;

import com.play.data.UserData;
import com.play.data.UserRoleData;

public class UserRoleView extends UserRoleData {
    private UserData user;

    public UserRoleView(UserRoleData userRole) {
        super(userRole);
    }

    public UserRoleView(UserRoleData userRole, UserData user) {
        super(userRole);
        this.user = user;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }
}
