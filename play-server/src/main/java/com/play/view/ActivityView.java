package com.play.view;

import com.play.data.ActivityData;
import com.play.data.ActivityTypeData;

public class ActivityView extends ActivityData {
    private ActivityTypeData ActivityType;

    public ActivityView(ActivityData activity) {
        super(activity);
    }

    public ActivityView(ActivityData activity, ActivityTypeData ActivityType){
        super(activity);
        this.ActivityType = ActivityType;
    }

    public ActivityTypeData getActivityType() {
        return ActivityType;
    }

    public void setActivityType(ActivityTypeData activityType) {
        ActivityType = activityType;
    }
}
