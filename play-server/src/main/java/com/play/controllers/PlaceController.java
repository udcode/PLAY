package com.play.controllers;

import com.play.data.PlaceActivityData;
import com.play.data.PlaceData;
import com.play.logic.PlaceLogic;
import com.play.view.PlaceActivityView;
import com.play.view.PlaceOwnerView;
import com.play.view.PlaceView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class PlaceController {
    @Autowired
    private PlaceLogic placeLogic;

    @RequestMapping(value = "/getAllPlaces", method = RequestMethod.POST)
    public Collection<PlaceView> getAllPlaces() {
        return placeLogic.getAllPlaces();
    }

    @RequestMapping(value = "/findPlacesByPlaceOwner", method = RequestMethod.POST)
    public Collection<PlaceView> findPlacesByPlaceOwner(@RequestParam(value="placeOwnerId", required = true) int placeOwnerId) {
        return  placeLogic.findPlacesByPlaceOwner(placeOwnerId);
    }

    @RequestMapping(value = "/getPlace", method = RequestMethod.POST)
    public PlaceView getPlace(@RequestParam(value="placeId", required = true) int placeId) {
        return placeLogic.getPlace(placeId);
    }

    @RequestMapping(value = "/createPlace", method = RequestMethod.POST)
    public PlaceView createPlace(@RequestBody PlaceData place,
                                 @RequestParam(value="placeOwnerId", required = true) int placeOwnerId) {
        return placeLogic.createPlace(place, placeOwnerId);
    }

    @RequestMapping(value = "/updatePlace", method = RequestMethod.POST)
    public PlaceView updatePlace(@RequestBody PlaceData place,
                                 @RequestParam(value="placeOwnerId", required = true) int placeOwnerId) {
        return placeLogic.updatePlace(place, placeOwnerId);
    }

    @RequestMapping(value = "/deletePlace", method = RequestMethod.POST)
    public void deletePlace(@RequestParam(value="placeId", required = true) int placeId) {
        placeLogic.deletePlace(placeId);
    }

    @RequestMapping(value = "/getAllPlaceOwners", method = RequestMethod.POST)
    public Collection<PlaceOwnerView> getAllPlaceOwners() {
        return placeLogic.getAllPlaceOwners();
    }

    @RequestMapping(value = "/getPlaceOwner", method = RequestMethod.POST)
    public PlaceOwnerView getPlaceOwner(@RequestParam(value="placeOwnerId", required = true) int placeOwnerId) {
        return placeLogic.getPlaceOwner(placeOwnerId);
    }

    @RequestMapping(value = "/getPlaceOwnerByUser", method = RequestMethod.POST)
    public PlaceOwnerView getPlaceOwnerByUser(@RequestParam(value="userId", required = true) int userId) {
        return placeLogic.getPlaceOwnerByUser(userId);
    }

    @RequestMapping(value = "/findPlaceActivitiesByPlace", method = RequestMethod.POST)
    public Collection<PlaceActivityView> findPlaceActivitiesByPlace(@RequestParam(value="placeId", required = true) int placeId) {
        return placeLogic.findPlaceActivitiesByPlace(placeId);
    }

    @RequestMapping(value = "/findPlaceActivitiesByActivity", method = RequestMethod.POST)
    public Collection<PlaceActivityView> findPlaceActivitiesByActivity(@RequestParam(value="activityId", required = true) int activityId) {
        return placeLogic.findPlaceActivitiesByActivity(activityId);
    }

    @RequestMapping(value = "/createPlaceActivity", method = RequestMethod.POST)
    public PlaceActivityView createPlaceActivity(@RequestBody PlaceActivityData placeActivity,
                                                 @RequestParam(value="placeId", required = true) int placeId,
                                                 @RequestParam(value="activityId", required = true) int activityId) {
        return placeLogic.createPlaceActivity(placeActivity, placeId, activityId);
    }

    @RequestMapping(value = "/updatePlaceActivity", method = RequestMethod.POST)
    public PlaceActivityView updatePlaceActivity(@RequestBody PlaceActivityData placeActivity,
                                                 @RequestParam(value="placeId", required = true) int placeId,
                                                 @RequestParam(value="activityId", required = true) int activityId) {
        return placeLogic.updatePlaceActivity(placeActivity, placeId, activityId);
    }

    @RequestMapping(value = "/deletePlaceActivity", method = RequestMethod.POST)
    public void deletePlaceActivity(@RequestParam(value="placeActivityId", required = true) int placeActivityId) {
        placeLogic.deletePlaceActivity(placeActivityId);
    }
}
