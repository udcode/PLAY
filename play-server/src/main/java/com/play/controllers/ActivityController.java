package com.play.controllers;

import com.play.data.ActivityData;
import com.play.data.ActivityTypeData;
import com.play.logic.ActivityLogic;
import com.play.view.ActivityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class ActivityController {
    @Autowired
    private ActivityLogic activityLogic;

    @RequestMapping(value = "/getAllActivities", method = RequestMethod.POST)
    public Collection<ActivityView> getAllActivities() {
        return activityLogic.getAllActivities();
    }

    @RequestMapping(value = "/findActivityByActivityType", method = RequestMethod.POST)
    public Collection<ActivityData> findActivitiesByActivityType(@RequestParam(value="activityTypeId", required = true) int activityTypeId) {
        return activityLogic.findActivitiesByActivityType(activityTypeId);
    }

    @RequestMapping(value = "/getActivity", method = RequestMethod.POST)
    public ActivityView getActivity(@RequestParam(value="activityId", required = true) int activityId) {
        return activityLogic.getActivity(activityId);
    }

    @RequestMapping(value = "/createActivity", method = RequestMethod.POST)
    public ActivityView createActivity(@RequestBody ActivityData activity,
                                       @RequestParam(value="activityTypeId", required = true) int activityTypeId) {
        return activityLogic.createActivity(activity, activityTypeId);
    }

    @RequestMapping(value = "/updateActivity", method = RequestMethod.POST)
    public ActivityView updateActivity(@RequestBody ActivityData activity,
                                       @RequestParam(value="activityTypeId", required = true) int activityTypeId) {
        return activityLogic.updateActivity(activity, activityTypeId);
    }

    @RequestMapping(value = "/deleteActivity", method = RequestMethod.POST)
    public void deleteActivity(@RequestParam(value="activityId", required = true) int activityId) {
        activityLogic.deleteActivity(activityId);
    }

    @RequestMapping(value = "/getAllActivityTypes", method = RequestMethod.POST)
    public Collection<ActivityTypeData> getAllActivityTypes() {
        return activityLogic.getAllActivityTypes();
    }

    @RequestMapping(value = "/getActivityType", method = RequestMethod.POST)
    public ActivityTypeData getActivityType(@RequestParam(value="activityTypeId", required = true) int activityTypeId) {
        return activityLogic.getActivityType(activityTypeId);
    }

    @RequestMapping(value = "/createActivityType", method = RequestMethod.POST)
    public ActivityTypeData createActivityType(@RequestBody ActivityTypeData activityType) {
        return activityLogic.createActivityType(activityType);
    }

    @RequestMapping(value = "/updateActivityType", method = RequestMethod.POST)
    public ActivityTypeData updateActivityType(@RequestBody ActivityTypeData activityType) {
        return activityLogic.updateActivityType(activityType);
    }

    @RequestMapping(value = "/deleteActivityType", method = RequestMethod.POST)
    public void deleteActivityType(@RequestParam(value="activityTypeId", required = true) int activityTypeId) {
        activityLogic.deleteActivityType(activityTypeId);
    }
}
