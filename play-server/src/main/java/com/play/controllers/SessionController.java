package com.play.controllers;

import com.play.data.SessionData;
import com.play.data.SessionDefinitionData;
import com.play.data.SessionDefinitionScheduleData;
import com.play.logic.SessionLogic;
import com.play.view.SessionDefinitionScheduleView;
import com.play.view.SessionDefinitionView;
import com.play.view.SessionView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class SessionController {
    @Autowired
    private SessionLogic sessionLogic;

    @RequestMapping(value = "/getAllSessionDefinitions", method = RequestMethod.POST)
    public Collection<SessionDefinitionView> getAllSessionDefinitions() {
        return sessionLogic.getAllSessionDefinitions();
    }

    @RequestMapping(value = "/findSessionDefinitionsByInstructor", method = RequestMethod.POST)
    public Collection<SessionDefinitionView> findSessionDefinitionsByInstructor(@RequestParam(value = "instructorId", required = true) int instructorId) {
        return sessionLogic.findSessionDefinitionsByInstructor(instructorId);
    }

    @RequestMapping(value = "/findSessionDefinitionsByPlace", method = RequestMethod.POST)
    public Collection<SessionDefinitionView> findSessionDefinitionsByPlace(@RequestParam(value = "placeId", required = true) int placeId) {
        return sessionLogic.findSessionDefinitionsByPlace(placeId);
    }

    @RequestMapping(value = "/findSessionDefinitionsByMembership", method = RequestMethod.POST)
    public Collection<SessionDefinitionView> findSessionDefinitionsByMembership(@RequestParam(value = "membershipId", required = true) int membershipId) {
        return sessionLogic.findSessionDefinitionsByMembership(membershipId);
    }

    @RequestMapping(value = "/findSessionDefinitionsByActivity", method = RequestMethod.POST)
    public Collection<SessionDefinitionView> findSessionDefinitionsByActivity(@RequestParam(value = "activityId", required = true) int activityId) {
        return sessionLogic.findSessionDefinitionsByActivity(activityId);
    }

    @RequestMapping(value = "/getSessionDefinition", method = RequestMethod.POST)
    public SessionDefinitionView getSessionDefinition(@RequestParam(value = "sessionDefinitionId", required = true) int sessionDefinitionId) {
        return sessionLogic.getSessionDefinition(sessionDefinitionId);
    }

    @RequestMapping(value = "/createSessionDefinition", method = RequestMethod.POST)
    public SessionDefinitionView createSessionDefinition(@RequestBody SessionDefinitionData sessionDefinition,
                                                         @RequestParam(value = "instructorId", required = true) int instructorId,
                                                         @RequestParam(value = "placeId", required = true) int placeId,
                                                         @RequestParam(value = "membershipId", required = true) int membershipId,
                                                         @RequestParam(value = "activityId", required = true) int activityId) {
        return sessionLogic.createSessionDefinition(sessionDefinition, instructorId, placeId, membershipId, activityId);
    }

    @RequestMapping(value = "/updateSessionDefinition", method = RequestMethod.POST)
    public SessionDefinitionView updateSessionDefinition(@RequestBody SessionDefinitionData sessionDefinition,
                                                         @RequestParam(value = "instructorId", required = true) int instructorId,
                                                         @RequestParam(value = "placeId", required = true) int placeId,
                                                         @RequestParam(value = "membershipId", required = true) int membershipId,
                                                         @RequestParam(value = "activityId", required = true) int activityId) {
        return sessionLogic.updateSessionDefinition(sessionDefinition, instructorId, placeId, membershipId, activityId);
    }

    @RequestMapping(value = "/deleteSessionDefinition", method = RequestMethod.POST)
    public void deleteSessionDefinition(@RequestParam(value="sessionDefinitionId", required = true) int sessionDefinitionId) {
        sessionLogic.deleteSessionDefinition(sessionDefinitionId);
    }

    @RequestMapping(value = "/findSessionDefinitionSchedulesBySessionDefinition", method = RequestMethod.POST)
    public Collection<SessionDefinitionScheduleData> findSessionDefinitionSchedulesBySessionDefinition(@RequestParam(value = "sessionDefinitionId", required = true) int sessionDefinitionId) {
        return sessionLogic.findSessionDefinitionSchedulesBySessionDefinition(sessionDefinitionId);
    }

    @RequestMapping(value = "/getSessionDefinitionSchedule", method = RequestMethod.POST)
    public SessionDefinitionScheduleData getSessionDefinitionSchedule(@RequestParam(value = "sessionDefinitionScheduleId", required = true) int sessionDefinitionScheduleId) {
        return sessionLogic.getSessionDefinitionSchedule(sessionDefinitionScheduleId);
    }

    @RequestMapping(value = "/createSessionDefinitionSchedule", method = RequestMethod.POST)
    public SessionDefinitionScheduleView createSessionDefinitionSchedule(@RequestBody SessionDefinitionScheduleData sessionDefinitionSchedule,
                                                                         @RequestParam(value = "sessionDefinitionId", required = true) int sessionDefinitionId) {
        return sessionLogic.createSessionDefinitionSchedule(sessionDefinitionSchedule, sessionDefinitionId);
    }

    @RequestMapping(value = "/updateSessionDefinitionSchedule", method = RequestMethod.POST)
    public SessionDefinitionScheduleView updateSessionDefinitionSchedule(@RequestBody SessionDefinitionScheduleData sessionDefinitionSchedule,
                                                                         @RequestParam(value = "sessionDefinitionId", required = true) int sessionDefinitionId) {
        return sessionLogic.updateSessionDefinitionSchedule(sessionDefinitionSchedule, sessionDefinitionId);
    }

    @RequestMapping(value = "/deleteSessionDefinitionSchedule", method = RequestMethod.POST)
    public void deleteSessionDefinitionSchedule(@RequestParam(value="sessionDefinitionScheduleId", required = true) int sessionDefinitionScheduleId) {
        sessionLogic.deleteSessionDefinitionSchedule(sessionDefinitionScheduleId);
    }

    @RequestMapping(value = "/getAllSessions", method = RequestMethod.POST)
    public Collection<SessionView> getAllSessions() {
        return sessionLogic.getAllSessions();
    }

    @RequestMapping(value = "/findSessionsBySessionDefinition", method = RequestMethod.POST)
    public Collection<SessionView> findSessionsBySessionDefinition(@RequestParam(value = "sessionDefinitionId", required = true) int sessionDefinitionId) {
        return sessionLogic.findSessionsBySessionDefinition(sessionDefinitionId);
    }

    @RequestMapping(value = "/findSessionsByInstructor", method = RequestMethod.POST)
    public Collection<SessionView> findSessionsByInstructor(@RequestParam(value = "instructorId", required = true) int instructorId) {
        return sessionLogic.findSessionsByInstructor(instructorId);
    }

    @RequestMapping(value = "/findSessionsByPlace", method = RequestMethod.POST)
    public Collection<SessionView> findSessionsByPlace(@RequestParam(value = "placeId", required = true) int placeId) {
        return sessionLogic.findSessionsByPlace(placeId);
    }

    @RequestMapping(value = "/findSessionsByMembership", method = RequestMethod.POST)
    public Collection<SessionView> findSessionsByMembership(@RequestParam(value = "membershipId", required = true) int membershipId) {
        return sessionLogic.findSessionsByMembership(membershipId);
    }

    @RequestMapping(value = "/getSession", method = RequestMethod.POST)
    public SessionView getSession(@RequestParam(value = "sessionId", required = true) int sessionId) {
        return sessionLogic.getSession(sessionId);
    }

    @RequestMapping(value = "/createSession", method = RequestMethod.POST)
    public SessionView createSession(@RequestBody SessionData session,
                                     @RequestParam(value = "sessionDefinitionId", required = true) int sessionDefinitionId,
                                     @RequestParam(value = "instructorId", required = true) int instructorId,
                                     @RequestParam(value = "placeId", required = true) int placeId,
                                     @RequestParam(value = "membershipId", required = true) int membershipId) {
        return sessionLogic.createSession(session, sessionDefinitionId, instructorId, placeId, membershipId);
    }

    @RequestMapping(value = "/updateSession", method = RequestMethod.POST)
    public SessionView updateSession(@RequestBody SessionData session,
                                     @RequestParam(value = "sessionDefinitionId", required = true) int sessionDefinitionId,
                                     @RequestParam(value = "instructorId", required = true) int instructorId,
                                     @RequestParam(value = "placeId", required = true) int placeId,
                                     @RequestParam(value = "membershipId", required = true) int membershipId) {
        return sessionLogic.updateSession(session, sessionDefinitionId, instructorId, placeId, membershipId);
    }

    @RequestMapping(value = "/deleteSession", method = RequestMethod.POST)
    public void deleteSession(@RequestParam(value="sessionId", required = true) int sessionId) {
        sessionLogic.deleteSession(sessionId);
    }
}