package com.play.controllers;

import com.play.data.PlayerActivityLevelData;
import com.play.data.PlayerSessionData;
import com.play.logic.PlayerLogic;
import com.play.view.PlayerActivityLevelView;
import com.play.view.PlayerSessionView;
import com.play.view.PlayerView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class PlayerController {
    @Autowired
    private PlayerLogic playerLogic;

    @RequestMapping(value = "/getAllPlayers", method = RequestMethod.POST)
    public Collection<PlayerView> getAllPlayers() {
        return playerLogic.getAllPlayers();
    }

    @RequestMapping(value = "/findPlayersByMembership", method = RequestMethod.POST)
    public Collection<PlayerView> findPlayersByMembership(@RequestParam(value="membershipId", required = true) int membershipId) {
        return playerLogic.findPlayersByMembership(membershipId);
    }

    @RequestMapping(value = "/getPlayer", method = RequestMethod.POST)
    public PlayerView getPlayer(@RequestParam(value="playerId", required = true) int playerId) {
        return playerLogic.getPlayer(playerId);
    }

    @RequestMapping(value = "/getPlayerByUser", method = RequestMethod.POST)
    public PlayerView getPlayerByUser(@RequestParam(value="userId", required = true) int userId) {
        return playerLogic.getPlayerByUser(userId);
    }

    @RequestMapping(value = "/findPlayerActivityLevelsByPlayer", method = RequestMethod.POST)
    public Collection<PlayerActivityLevelView> findPlayerActivityLevelsByPlayer(@RequestParam(value="playerId", required = true) int playerId) {
        return playerLogic.findPlayerActivityLevelsByPlayer(playerId);
    }

    @RequestMapping(value = "/findPlayerActivityLevelsByActivity", method = RequestMethod.POST)
    public Collection<PlayerActivityLevelView> findPlayerActivityLevelsByActivity(@RequestParam(value="activityId", required = true) int activityId) {
        return playerLogic.findPlayerActivityLevelsByActivity(activityId);
    }

    @RequestMapping(value = "/getPlayerActivityLevel", method = RequestMethod.POST)
    public PlayerActivityLevelView getPlayerActivityLevel(@RequestParam(value="playerActivityLevelId", required = true) int playerActivityLevelId) {
        return playerLogic.getPlayerActivityLevel(playerActivityLevelId);
    }

    @RequestMapping(value = "/createPlayerActivityLevel", method = RequestMethod.POST)
    public PlayerActivityLevelView createPlayerActivityLevel(@RequestBody PlayerActivityLevelData playerActivityLevel,
                                                             @RequestParam(value="playerId", required = true) int playerId,
                                                             @RequestParam(value="activityId", required = true) int activityId) {
        return playerLogic.createPlayerActivityLevel(playerActivityLevel, playerId, activityId);
    }

    @RequestMapping(value = "/updatePlayerActivityLevel", method = RequestMethod.POST)
    public PlayerActivityLevelView updatePlayerActivityLevel(@RequestBody PlayerActivityLevelData playerActivityLevel,
                                                             @RequestParam(value="playerId", required = true) int playerId,
                                                             @RequestParam(value="activityId", required = true) int activityId) {
        return playerLogic.updatePlayerActivityLevel(playerActivityLevel, playerId, activityId);
    }


    @RequestMapping(value = "/deletePlayerActivityLevel", method = RequestMethod.POST)
    public void deletePlayerActivityLevel(@RequestParam(value="playerActivityLevelId", required = true) int playerActivityLevelId) {
        playerLogic.deletePlayerActivityLevel(playerActivityLevelId);
    }

    @RequestMapping(value = "/findPlayerSessionsByPlayer", method = RequestMethod.POST)
    public Collection<PlayerSessionView> findPlayerSessionsByPlayer(@RequestParam(value="playerId", required = true) int playerId) {
        return playerLogic.findPlayerSessionsByPlayer(playerId);
    }

    @RequestMapping(value = "/findPlayerSessionsBySession", method = RequestMethod.POST)
    public Collection<PlayerSessionView> findPlayerSessionsBySession(@RequestParam(value="sessionId", required = true) int sessionId) {
        return playerLogic.findPlayerSessionsBySession(sessionId);
    }

    @RequestMapping(value = "/createPlayerSession", method = RequestMethod.POST)
    public PlayerSessionView createPlayerSession(@RequestBody PlayerSessionData playerSession,
                                                 @RequestParam(value="playerId", required = true) int playerId,
                                                 @RequestParam(value="sessionId", required = true) int sessionId) {
        return playerLogic.createPlayerSession(playerSession, playerId, sessionId);
    }

    @RequestMapping(value = "/updatePlayerSession", method = RequestMethod.POST)
    public PlayerSessionView updatePlayerSession(@RequestBody PlayerSessionData playerSession,
                                                 @RequestParam(value="playerId", required = true) int playerId,
                                                 @RequestParam(value="sessionId", required = true) int sessionId) {
        return playerLogic.updatePlayerSession(playerSession, playerId, sessionId);
    }

    @RequestMapping(value = "/deletePlayerSession", method = RequestMethod.POST)
    public void deletePlayerSession(@RequestParam(value="playerSessionId", required = true) int playerSessionId) {
        playerLogic.deletePlayerSession(playerSessionId);
    }
}
