package com.play.controllers;

import com.play.logic.InstructorLogic;
import com.play.view.InstructorView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class InstructorController {
    @Autowired
    private InstructorLogic instructorLogic;

    @RequestMapping(value = "/getAllInstructors", method = RequestMethod.POST)
    public Collection<InstructorView> getAllInstructors() {
        return instructorLogic.getAllInstructors();
    }

    @RequestMapping(value = "/getInstructor", method = RequestMethod.POST)
    public InstructorView getInstructor(@RequestParam(value = "instructorId", required = true) int instructorId) {
        return instructorLogic.getInstructor(instructorId);
    }

    @RequestMapping(value = "/getInstructorByUser", method = RequestMethod.POST)
    public InstructorView getInstructorByUser(@RequestParam(value = "userId", required = true) int userId) {
        return instructorLogic.getInstructorByUser(userId);
    }
}