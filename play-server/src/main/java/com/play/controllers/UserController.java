package com.play.controllers;

import com.play.data.StaffMemberPermissionsEnum;
import com.play.data.UserData;
import com.play.data.UserEventData;
import com.play.data.UserRoleEnum;
import com.play.logic.UserLogic;
import com.play.view.UserEventView;
import com.play.view.UserView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
public class UserController {
    @Autowired
    private UserLogic userLogic;

    @RequestMapping(value = "/getAllUsers", method = RequestMethod.POST)
    public Collection<UserView> getAllUsers() {
        return userLogic.getAllUsers();
    }

    @RequestMapping(value = "/findUsersByReferenceUser", method = RequestMethod.POST)
    public Collection<UserView> findUsersByReferenceUser(@RequestParam(value="referenceUserId", required = true) int referenceUserId) {
        return userLogic.findUsersByReferenceUser(referenceUserId);
    }

    @RequestMapping(value = "/getUser", method = RequestMethod.POST)
    public UserView getUser(@RequestParam(value="userId", required = true) int userId) {
        return userLogic.getUser(userId);
    }

    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    public UserView createUser(@RequestBody UserData user,
                               @ModelAttribute("roles") ArrayList<String> roles,
                               @RequestParam(value="referenceUserId", required = false) Integer referenceUserId,
                               @RequestParam(value="playerMembershipId", required = false) Integer playerMembershipId,
                               @RequestParam(value="staffMemberPermissions", required = false) StaffMemberPermissionsEnum staffMemberPermissions) {
        Collection<UserRoleEnum> roleEnums = roles == null || roles.isEmpty() ?
                null : roles.stream().map(r -> UserRoleEnum.valueOf(r)).collect(Collectors.toList());
        return userLogic.createUser(user, roleEnums, referenceUserId, playerMembershipId, staffMemberPermissions);
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public UserView updateUser(@RequestBody UserData user,
                               @ModelAttribute("roles") ArrayList<String> roles,
                               @RequestParam(value="referenceUserId", required = false) Integer referenceUserId,
                               @RequestParam(value="playerMembershipId", required = false) Integer playerMembershipId,
                               @RequestParam(value="staffMemberPermissions", required = false) StaffMemberPermissionsEnum staffMemberPermissions) {

        Collection<UserRoleEnum> roleEnums = roles == null || roles.isEmpty() ?
                null : roles.stream().map(r -> UserRoleEnum.valueOf(r)).collect(Collectors.toList());
        return userLogic.updateUser(user, roleEnums, referenceUserId, playerMembershipId, staffMemberPermissions);
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    public void deleteUser(@RequestParam(value="userId", required = true) int userId) {
        userLogic.deleteUser(userId);
    }

    @RequestMapping(value = "/getAllUserEvents", method = RequestMethod.POST)
    public Collection<UserEventView> getAllUserEvents() {
        return userLogic.getAllUserEvents();
    }

    @RequestMapping(value = "/findUserEventsByUser", method = RequestMethod.POST)
    public Collection<UserEventView> findUserEventsByUser(@RequestParam(value="userId", required = true) int userId) {
        return userLogic.findUserEventsByUser(userId);
    }

    @RequestMapping(value = "/findUserEventsByStaffMember", method = RequestMethod.POST)
    public Collection<UserEventView> findUserEventsByStaffMember(@RequestParam(value="staffMemberId", required = true) int staffMemberId) {
        return userLogic.findUserEventsByStaffMember(staffMemberId);
    }

    @RequestMapping(value = "/getUserEvent", method = RequestMethod.POST)
    public UserEventView getUserEvent(@RequestParam(value="userEventId", required = true) int userEventId) {
        return userLogic.getUserEvent(userEventId);
    }

    @RequestMapping(value = "/createUserEvent", method = RequestMethod.POST)
    public UserEventView createUserEvent(@RequestBody UserEventData userEvent,
                                         @RequestParam(value="userId", required = true) int userId,
                                         @RequestParam(value="staffMemberId", required = true) int staffMemberId) {
        return userLogic.createUserEvent(userEvent, userId, staffMemberId);
    }

    @RequestMapping(value = "/updateUserEvent", method = RequestMethod.POST)
    public UserEventView updateUserEvent(@RequestBody UserEventData userEvent,
                                         @RequestParam(value="userId", required = true) int userId,
                                         @RequestParam(value="staffMemberId", required = true) int staffMemberId) {
        return userLogic.updateUserEvent(userEvent, userId, staffMemberId);
    }

    @RequestMapping(value = "/deleteUserEvent", method = RequestMethod.POST)
    public void deleteUserEvent(@RequestParam(value="userEventId", required = true) int userEventId) {
        userLogic.deleteUserEvent(userEventId);
    }
}
