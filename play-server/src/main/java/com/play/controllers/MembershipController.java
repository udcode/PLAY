package com.play.controllers;

import com.play.data.MembershipData;
import com.play.logic.MembershipLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class MembershipController {
    @Autowired
    private MembershipLogic membershipLogic;

    @RequestMapping(value = "/getAllMemberships", method = RequestMethod.POST)
    public Collection<MembershipData> getAllMemberships() {
        return membershipLogic.getAllMemberships();
    }

    @RequestMapping(value = "/getMembership", method = RequestMethod.POST)
    public MembershipData getMembership(@RequestParam(value = "membershipId", required = true) int membershipId) {
        return membershipLogic.getMembership(membershipId);
    }

    @RequestMapping(value = "/createMembership", method = RequestMethod.POST)
    public MembershipData createMembership(@RequestBody MembershipData membership) {
        return membershipLogic.createMembership(membership);
    }

    @RequestMapping(value = "/updateMembership", method = RequestMethod.POST)
    public MembershipData updateMembership(@RequestBody MembershipData membership) {
        return membershipLogic.updateMembership(membership);
    }

    @RequestMapping(value = "/deleteMembership", method = RequestMethod.POST)
    public void deleteMembership(@RequestParam(value="membershipId", required = true) int membershipId) {
        membershipLogic.deleteMembership(membershipId);
    }
}