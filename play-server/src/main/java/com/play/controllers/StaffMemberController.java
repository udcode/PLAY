package com.play.controllers;

import com.play.logic.StaffMemberLogic;
import com.play.view.StaffMemberView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class StaffMemberController {
    @Autowired
    private StaffMemberLogic staffMemberLogic;

    @RequestMapping(value = "/getAllStaffMembers", method = RequestMethod.POST)
    public Collection<StaffMemberView> getAllStaffMembers() {
        return staffMemberLogic.getAllStaffMembers();
    }

    @RequestMapping(value = "/getStaffMember", method = RequestMethod.POST)
    public StaffMemberView getStaffMember(@RequestParam(value="staffMemberId", required = true) int staffMemberId) {
        return staffMemberLogic.getStaffMember(staffMemberId);
    }

    @RequestMapping(value = "/getStaffMemberByUser", method = RequestMethod.POST)
    public StaffMemberView getStaffMemberByUser(@RequestParam(value="userId", required = true) int userId) {
        return staffMemberLogic.getStaffMemberByUser(userId);
    }
}
