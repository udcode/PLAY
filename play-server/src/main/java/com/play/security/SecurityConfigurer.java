package com.play.security;

import com.play.utils.ServerProperties;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {
    private static final String DB_USERS_TABLE = "users";

    private static final String DB_EMAIL_FIELD = "email";
    private static final String DB_PASSWORD_FIELD = "password";
    private static final String DB_ACTIVE_FIELD = "active";

    private static final String FORM_USER_NAME_FIELD = "userName";
    private static final String FORM_PASSWORD_FIELD = "password";

    private static final String LOGIN_PAGE_URL = "/login";

    @Autowired
    private ServerProperties serverProperties;

    @Autowired
    @Qualifier("jdbcDataSource")
    private BasicDataSource dataSource;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder authManager) throws Exception {
        StringBuilder sbUsersByUsernameQuery = new StringBuilder();
        sbUsersByUsernameQuery.append("SELECT ");
        sbUsersByUsernameQuery.append(DB_EMAIL_FIELD);
        sbUsersByUsernameQuery.append(", CONCAT('{bcrypt}', ");
        sbUsersByUsernameQuery.append(DB_PASSWORD_FIELD);
        sbUsersByUsernameQuery.append("), ");
        sbUsersByUsernameQuery.append(DB_ACTIVE_FIELD);
        sbUsersByUsernameQuery.append(" FROM ");
        sbUsersByUsernameQuery.append(DB_USERS_TABLE);
        sbUsersByUsernameQuery.append(" WHERE ");
        sbUsersByUsernameQuery.append(DB_EMAIL_FIELD);
        sbUsersByUsernameQuery.append("=?");

        authManager
                .jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(sbUsersByUsernameQuery.toString())
                .authoritiesByUsernameQuery("SELECT users.email, users_roles.role FROM users INNER JOIN users_roles ON users.id = users_roles.user_id WHERE users.email=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        boolean enabled = serverProperties.getProperty("enable_security").equals("true");

        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.AuthorizedUrl authorizedUrl =
                http
                        .formLogin()
                        .loginPage(LOGIN_PAGE_URL)
                        .usernameParameter(FORM_USER_NAME_FIELD)
                        .passwordParameter(FORM_PASSWORD_FIELD)
                        .permitAll()
                        .and()
                        .authorizeRequests()
                        .anyRequest();

        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry =
                enabled ? authorizedUrl.authenticated() : authorizedUrl.permitAll();

        expressionInterceptUrlRegistry
                .and()
                .httpBasic()
                .and()
                .csrf()
                .disable();
    }
}
