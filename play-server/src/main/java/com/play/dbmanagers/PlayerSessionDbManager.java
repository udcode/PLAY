package com.play.dbmanagers;

import com.play.data.PlayerSessionData;
import com.play.view.PlayerSessionView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class PlayerSessionDbManager extends BaseDbManager<PlayerSessionData, PlayerSessionView> {
    @Autowired
    private PlayerDbManager playerDbManager;
    @Autowired
    private SessionDbManager sessionDbManager;

    public Collection<PlayerSessionView> findViewsByPlayerId(int playerId) {
        return findViewsByIntField(playerDbManager.getDefaultForeignKey(), playerId);
    }

    public Collection<PlayerSessionView> findViewsBySessionId(int sessionId) {
        return findViewsByIntField(sessionDbManager.getDefaultForeignKey(), sessionId);
    }

    public PlayerSessionView createPlayerSession(PlayerSessionData playerSession, int playerId, int sessionId) {
        return create(playerSession, playerId, sessionId);
    }

    public PlayerSessionView updatePlayerSession(PlayerSessionData playerSession, int playerId, int sessionId) {
        return update(playerSession, playerId, sessionId);
    }

    @Override
    protected PlayerSessionData newData() {
        return new PlayerSessionData();
    }

    @Override
    protected PlayerSessionView newView(PlayerSessionData playerSession) {
        return new PlayerSessionView(playerSession);
    }

    @Override
    protected String getPrimaryTableName() {
        return "players_sessions";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("player_arrived", "PlayerArrived", FieldType.bool));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, playerDbManager, true));
        foreignTableReferences.add(new ForeignTableReference(tableName, sessionDbManager, true));
        return foreignTableReferences;
    }
}
