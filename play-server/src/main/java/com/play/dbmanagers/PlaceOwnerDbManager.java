package com.play.dbmanagers;

import com.play.data.PlaceOwnerData;
import com.play.view.PlaceOwnerView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PlaceOwnerDbManager extends BaseEntityDbManager<PlaceOwnerData, PlaceOwnerView> {
    @Autowired
    private UserDbManager userDbManager;

    public PlaceOwnerView getViewByUserId(int userId) {
        return getViewByIntField(userDbManager.getDefaultForeignKey(), userId);
    }

    public PlaceOwnerView createPlaceOwner(PlaceOwnerData placeOwner, int userId) {
        return create(placeOwner, userId);
    }

    public PlaceOwnerView updatePlaceOwner(PlaceOwnerData placeOwner, int userId) {
        return update(placeOwner, userId);
    }

    @Override
    protected PlaceOwnerData newData() {
        return new PlaceOwnerData();
    }

    @Override
    protected PlaceOwnerView newView(PlaceOwnerData placeOwner) {
        return new PlaceOwnerView(placeOwner);
    }

    @Override
    protected String getPrimaryTableName() {
        return "place_owners";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "place_owner_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "PlaceOwner";
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, userDbManager.getDefaultForeignKey(), userDbManager.getPrimaryTableName(), "User", userDbManager, false));
        return foreignTableReferences;
    }
}