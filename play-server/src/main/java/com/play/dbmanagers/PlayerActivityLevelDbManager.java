package com.play.dbmanagers;

import com.play.data.PlayerActivityLevelData;
import com.play.view.PlayerActivityLevelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class PlayerActivityLevelDbManager extends BaseDbManager<PlayerActivityLevelData, PlayerActivityLevelView> {
    @Autowired
    private PlayerDbManager playerDbManager;
    @Autowired
    private ActivityDbManager activityDbManager;

    public Collection<PlayerActivityLevelView> findViewsByPlayerId(int playerId) {
        return findViewsByIntField(playerDbManager.getDefaultForeignKey(), playerId);
    }

    public Collection<PlayerActivityLevelView> findViewsByActivityId(int activityId) {
        return findViewsByIntField(activityDbManager.getDefaultForeignKey(), activityId);
    }

    public PlayerActivityLevelView createPlayerActivityLevel(PlayerActivityLevelData playerActivityLevel, int playerId, int activityId) {
        return create(playerActivityLevel, playerId, activityId);
    }

    public PlayerActivityLevelView updatePlayerActivityLevel(PlayerActivityLevelData playerActivityLevel, int playerId, int activityId) {
        return update(playerActivityLevel, playerId, activityId);
    }

    @Override
    protected PlayerActivityLevelData newData() {
        return new PlayerActivityLevelData();
    }

    @Override
    protected PlayerActivityLevelView newView(PlayerActivityLevelData playerActivityLevel) {
        return new PlayerActivityLevelView(playerActivityLevel);
    }

    @Override
    protected String getPrimaryTableName() {
        return "player_activity_levels";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("player_activity_level", "PlayerActivityLevel", FieldType.number));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, playerDbManager, true));
        foreignTableReferences.add(new ForeignTableReference(tableName, activityDbManager, true));
        return foreignTableReferences;
    }
}