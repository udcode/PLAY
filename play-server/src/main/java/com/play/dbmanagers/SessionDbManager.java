package com.play.dbmanagers;

import com.play.data.SessionData;
import com.play.view.SessionView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class SessionDbManager extends BaseEntityDbManager<SessionData, SessionView> {
    @Autowired
    private SessionDefinitionDbManager sessionDefinitionDbManager;
    @Autowired
    private InstructorDbManager instructorDbManager;
    @Autowired
    private PlaceDbManager placeDbManager;
    @Autowired
    private MembershipDbManager membershipDbManager;

    public Collection<SessionView> findViewsBySessionDefinitionId(int sessionDefinitionId) {
        return findViewsByIntField(sessionDefinitionDbManager.getDefaultForeignKey(), sessionDefinitionId);
    }

    public Collection<SessionView> findViewsByInstructorId(int instructorId) {
        return findViewsByIntField(instructorDbManager.getDefaultForeignKey(), instructorId);
    }

    public Collection<SessionView> findViewsByPlaceId(int placeId) {
        return findViewsByIntField(placeDbManager.getDefaultForeignKey(), placeId);
    }

    public Collection<SessionView> findViewsByMembershipId(int membershipId) {
        return findViewsByIntField(membershipDbManager.getDefaultForeignKey(), membershipId);
    }

    public SessionView createSession(SessionData session, int sessionDefinitionId, int instructorId, int placeId, int membershipId) {
        return create(session, sessionDefinitionId, instructorId, placeId, membershipId);
    }

    public SessionView updateSession(SessionData session, int sessionDefinitionId, int instructorId, int placeId, int membershipId) {
        return update(session, sessionDefinitionId, instructorId, placeId, membershipId);
    }

    @Override
    protected SessionData newData() {
        return new SessionData();
    }

    @Override
    protected SessionView newView(SessionData session) {
        return new SessionView(session);
    }

    @Override
    protected String getPrimaryTableName() {
        return "sessions";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "session_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "Session";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("min_players", "MinPlayers", FieldType.number));
        fields.add(new Field("max_players", "MaxPlayers", FieldType.number));
        fields.add(new Field("activity_level", "ActivityLevel", FieldType.number));
        fields.add(new Field("start_time", "StartTime", FieldType.dateTime));
        fields.add(new Field("end_time", "EndTime", FieldType.dateTime));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, sessionDefinitionDbManager, false));
        foreignTableReferences.add(new ForeignTableReference(tableName, instructorDbManager, true));
        foreignTableReferences.add(new ForeignTableReference(tableName, placeDbManager, false));
        foreignTableReferences.add(new ForeignTableReference(tableName, membershipDbManager, false));
        return foreignTableReferences;
    }
}