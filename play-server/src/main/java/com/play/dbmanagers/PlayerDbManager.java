package com.play.dbmanagers;

import com.play.data.PlayerData;
import com.play.view.PlayerView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class PlayerDbManager extends BaseEntityDbManager<PlayerData, PlayerView> {
    @Autowired
    private UserDbManager userDbManager;
    @Autowired
    private MembershipDbManager membershipDbManager;

    public Collection<PlayerView> findViewsByMembershipId(int membershipId) {
        return findViewsByIntField(membershipDbManager.getDefaultForeignKey(), membershipId);
    }

    public PlayerView getViewByUserId(int userId) {
        return getViewByIntField(userDbManager.getDefaultForeignKey(), userId);
    }

    public PlayerView createPlayer(PlayerData player, int userId, int membershipId) {
        return create(player, userId, membershipId);
    }

    public PlayerView updatePlayer(PlayerData player, int userId, int membershipId) {
        return update(player, userId, membershipId);
    }

    @Override
    protected PlayerData newData() {
        return new PlayerData();
    }

    @Override
    protected PlayerView newView(PlayerData player) {
        return new PlayerView(player);
    }

    @Override
    protected String getPrimaryTableName() {
        return "players";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "player_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "Player";
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<ForeignTableReference>();
        foreignTableReferences.add(new ForeignTableReference(tableName, userDbManager, false));
        foreignTableReferences.add(new ForeignTableReference(tableName, membershipDbManager, false));
        return foreignTableReferences;
    }
}