package com.play.dbmanagers;

import com.play.utils.EnumUtil;
import com.play.data.BaseData;
import com.play.data.BaseEnum;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.lang.reflect.Method;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.stream.Collectors;

public abstract class BaseDbManager<DATA extends BaseData, VIEW extends DATA> {
    private static Logger log = LogManager.getLogger(BaseDbManager.class);
    protected static final String DB_FIELD_ID = "id";
    protected static final String DB_FIELD_NAME_SEPARATOR = "_";
    private static final String IS_METHOD_PREFIX = "is";
    private static final String GETTER_METHOD_PREFIX = "get";
    private static final String SETTER_METHOD_PREFIX = "set";
    private static final String ENUM_METHOD_NAME_GET_BY_NUMBER_VALUE = "getByNumberValue";

    @Autowired
    @Qualifier("jdbcDataSource")
    private BasicDataSource dataSource;

    protected Connection getConnection() {
        return DataSourceUtils.getConnection(dataSource);
    }

    public DATA getDataById(int id) {
        try {
            Connection connection = getConnection();
            DATA baseData = null;
            PreparedStatement statement = prepareStatementGetDataById(connection, id);
            statement.executeQuery();
            ResultSet resultSet = statement.getResultSet();
            if (resultSet.next()) {
                baseData = fetchData(getPrimaryTableName(), resultSet);
            }
            return baseData;
        }
        catch (SQLException sqlException) {
            throw new UncheckedWrapperException(sqlException);
        }
    }

    public VIEW getViewById(int id) {
        try {
            Connection connection = getConnection();
            VIEW view = null;
            PreparedStatement statement = prepareStatementGetViewById(connection, id);
            statement.executeQuery();
            ResultSet resultSet = statement.getResultSet();
            if (resultSet.next()) {
                view = fetchView(resultSet);
            }
            return view;
        }
        catch (SQLException sqlException) {
            throw new UncheckedWrapperException(sqlException);
        }
    }

    public Collection<DATA> getAllDatas() {
        try {
            Connection connection = getConnection();
            List<DATA> datas = new ArrayList<DATA>();
            PreparedStatement statement = prepareStatementGetAllDatas(connection);
            statement.executeQuery();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                DATA data = fetchData(getPrimaryTableName(), resultSet);
                datas.add(data);
            }
            return datas;
        }
        catch (SQLException sqlException) {
            throw new UncheckedWrapperException(sqlException);
        }
    }

    public Collection<VIEW> getAllViews()  {
        try {
            Connection connection = getConnection();
            List<VIEW> views = new ArrayList<VIEW>();
            PreparedStatement statement = prepareStatementGetAllViews(connection);
            statement.executeQuery();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                VIEW view = fetchView(resultSet);
                views.add(view);
            }
            return views;
        }
        catch (SQLException sqlException) {
            throw new UncheckedWrapperException(sqlException);
        }
    }

    protected Collection<DATA> findDatasByIntField(String fieldName, int fieldValue) {
        try {
            Connection connection = getConnection();
            List<DATA> datas = new ArrayList<DATA>();
            PreparedStatement statement = prepareStatementGetDatasByIntField(connection, fieldName, fieldValue);
            statement.executeQuery();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                DATA data = fetchData(getPrimaryTableName(), resultSet);
                datas.add(data);
            }
            return datas;
        }
        catch (SQLException sqlException) {
            throw new UncheckedWrapperException(sqlException);
        }
    }

    protected Collection<VIEW> findViewsByIntField(String field, int value) {
        try {
            Connection connection = getConnection();
            List<VIEW> views = new ArrayList<VIEW>();
            PreparedStatement statement = prepareStatementGetViewsByIntField(connection, field, value);
            statement.executeQuery();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                VIEW view = fetchView(resultSet);
                views.add(view);
            }
            return views;
        }
        catch (SQLException sqlException) {
            throw new UncheckedWrapperException(sqlException);
        }
    }

    protected VIEW getViewByIntField(String field, int value) {
        Collection<VIEW> views = findViewsByIntField(field, value);
        if ((views == null) || views.isEmpty()) {
            return null;
        }
        return views.iterator().next();
    }

    protected VIEW create(DATA baseData, Integer... foreignKeys) {
        return createFromView(newView(baseData, foreignKeys));
    }

    protected VIEW createFromView(VIEW view) {
        try {
            Connection connection = getConnection();
            PreparedStatement statement = prepareStatementCreateFromView(connection);
            fillStatementFromView(statement, view, true);
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();
            int id = resultSet.getInt(1);
            view.setId(id);
            return getViewById(id);
        }
        catch (SQLException sqlException) {
            throw new UncheckedWrapperException(sqlException);
        }
    }

    protected VIEW update(DATA baseData, Integer... foreignKeys) {
        return updateFromView(newView(baseData, foreignKeys));
    }

    protected VIEW updateFromView(VIEW view) {
        try {
            Connection connection = getConnection();
            PreparedStatement statement = prepareStatementUpdateFromView(connection);
            int iField = fillStatementFromView(statement, view, false);
            statement.setObject(++iField, view.getId());
            statement.executeUpdate();
            return getViewById(view.getId());
        }
        catch (SQLException sqlException) {
            throw new UncheckedWrapperException(sqlException);
        }
    }

    public void deleteById (int id) {
        deleteByIntField(DB_FIELD_ID, id);
    }

    public void deleteByIntField (String fieldName, int fieldValue) {
        try {
            Connection connection = getConnection();
            PreparedStatement statement = prepareStatementDeleteByIntField(connection, fieldName, fieldValue);
            statement.executeUpdate();
        }
        catch (SQLException sqlException) {
            throw new UncheckedWrapperException(sqlException);
        }
    }

    protected PreparedStatement prepareStatementGetDataById (Connection connection, int id) throws SQLException {
        return prepareStatementGetDatasByIntField(connection, DB_FIELD_ID, id);
    }

    protected PreparedStatement prepareStatementGetAllDatas (Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + getPrimaryTableName());
        return statement;
    }

    protected PreparedStatement prepareStatementGetDatasByIntField (Connection connection, String fieldName, int fieldValue) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + getPrimaryTableName() + " WHERE " + fieldName +" = ?");
        statement.setInt(1, fieldValue);
        return statement;
    }

    protected PreparedStatement prepareStatementGetViewById (Connection connection, int id) throws SQLException {
        return prepareStatementGetViewsByIntField(connection, DB_FIELD_ID, id);
    }

    protected PreparedStatement prepareStatementGetAllViews (Connection connection) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM ");
        sb.append(getPrimaryTableName());
        List<ForeignTableReference> foreignTableReferences = getForeignTableReferencesIncludingIndirectReferences();
        if ((foreignTableReferences != null) && (!foreignTableReferences.isEmpty())) {
            for (ForeignTableReference foreignTableReference : foreignTableReferences) {
                sb.append(foreignTableReference.isNullPermitted() ? " LEFT JOIN " : " INNER JOIN ");
                sb.append(foreignTableReference.getForeignTable());
                String foreignTableAs = calcForeignTableAlias(foreignTableReference.getReferrerTable(), foreignTableReference.getForeignTableAlias());
                if (foreignTableAs != null) {
                    sb.append(" AS ");
                    sb.append(foreignTableAs);
                }
                sb.append(" ON ");
                sb.append(foreignTableReference.getReferrerTable());
                sb.append(".");
                sb.append(foreignTableReference.getReferrerTableForeignKey());
                sb.append(" = ");
                sb.append(foreignTableAs == null ? foreignTableReference.getForeignTable() : foreignTableAs);
                sb.append(".");
                sb.append(foreignTableReference.getForeignTableKey());
            }
        }
        sb.append(" ORDER BY ");
        sb.append(getPrimaryTableName());
        sb.append(".");
        sb.append(DB_FIELD_ID);
        PreparedStatement statement = connection.prepareStatement(sb.toString());
        return statement;
    }

    protected PreparedStatement prepareStatementGetViewsByIntField (Connection connection, String fieldName, int fieldValue) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM ");
        sb.append(getPrimaryTableName());
        List<ForeignTableReference> foreignTableReferences = getForeignTableReferencesIncludingIndirectReferences();
        if ((foreignTableReferences != null) && (!foreignTableReferences.isEmpty())) {
            for (ForeignTableReference foreignTableReference : foreignTableReferences) {
                sb.append(foreignTableReference.nullPermitted ? " LEFT JOIN " : " INNER JOIN ");
                sb.append(foreignTableReference.getForeignTable());
                String foreignTableAs = calcForeignTableAlias(foreignTableReference.getReferrerTable(), foreignTableReference.getForeignTableAlias());
                if (foreignTableAs != null) {
                    sb.append(" AS ");
                    sb.append(foreignTableAs);
                }
                sb.append(" ON ");
                sb.append(foreignTableReference.getReferrerTable());
                sb.append(".");
                sb.append(foreignTableReference.getReferrerTableForeignKey());
                sb.append(" = ");
                sb.append(foreignTableAs == null ? foreignTableReference.getForeignTable() : foreignTableAs);
                sb.append(".");
                sb.append(foreignTableReference.getForeignTableKey());
            }
        }
        sb.append(" WHERE ");
        sb.append(getPrimaryTableName());
        sb.append(".");
        sb.append(fieldName);
        sb.append(" = ?");

        PreparedStatement statement = connection.prepareStatement(sb.toString());
        statement.setInt(1, fieldValue);
        return statement;
    }

    protected PreparedStatement prepareStatementCreateFromView (Connection connection) throws SQLException {
        StringBuilder sbFieldNames = new StringBuilder();
        StringBuilder sbFieldValues = new StringBuilder();
        List<String> dbFields = calcDbFields(true);
        boolean firstField = true;
        for (String dbField : dbFields) {
            if (firstField) {
                firstField = false;
            }
            else {
                sbFieldNames.append(", ");
                sbFieldValues.append(", ");
            }
            sbFieldNames.append(dbField);
            sbFieldValues.append("?");
        }
        StringBuilder sbStatement = new StringBuilder();
        sbStatement.append("INSERT INTO ");
        sbStatement.append(getPrimaryTableName());
        sbStatement.append(" (");
        sbStatement.append(sbFieldNames);
        sbStatement.append(") VALUES (");
        sbStatement.append(sbFieldValues);
        sbStatement.append(")");
        return connection.prepareStatement(sbStatement.toString(), Statement.RETURN_GENERATED_KEYS);
    }

    protected PreparedStatement prepareStatementUpdateFromView (Connection connection) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(getPrimaryTableName());
        sb.append(" SET ");
        List<String> dbFields = calcDbFields(false);
        boolean firstField = true;
        for (String dbField : dbFields) {
            if (firstField) {
                firstField = false;
            }
            else {
                sb.append(", ");
            }
            sb.append(dbField);
            sb.append("=?");
        }
        sb.append(" WHERE ");
        sb.append(DB_FIELD_ID);
        sb.append(" = ?");
        return connection.prepareStatement(sb.toString());
    }

    protected PreparedStatement prepareStatementDeleteByIntField (Connection connection, String fieldName, int fieldValue) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("DELETE FROM " + getPrimaryTableName() + " WHERE " + fieldName + "=?");
        statement.setInt(1, fieldValue);
        return statement;
    }

    protected abstract DATA newData();

    protected abstract VIEW newView(DATA baseData);

    protected VIEW newView(DATA baseData, Integer... foreignKeys) {
        VIEW view = newView(baseData);
        int iForeignKey = 0;
        for (ForeignTableReference foreignTableReference : getForeignTableReferences()) {
            setForeignObjectInView(view, foreignTableReference.getDataObjectMethodName(), foreignTableReference.getForeignDbManager(), foreignTableReference.foreignEntityView, foreignKeys[iForeignKey++]);
        }
        return view;
    }

    protected DATA fetchData(String tableName, ResultSet resultSet) throws SQLException {
        DATA baseData = newData();
        fetchData(baseData, tableName, resultSet);
        return baseData;
    }

    protected void fetchData(DATA baseData, String tableName, ResultSet resultSet) throws SQLException {
        for(Field field : getFields()) {
            switch (field.getType()) {
                case number:
                    getNumberField(baseData, field.getDataObjectMethodName(), resultSet, tableName, field.getDbName());
                    break;
                case text:
                case loggedInUser:
                    getTextField(baseData, field.getDataObjectMethodName(), resultSet, tableName, field.getDbName());
                    break;
                case bool:
                    getBoolField(baseData, field.getDataObjectMethodName(), resultSet, tableName, field.getDbName());
                    break;
                case date:
                    getDateField(baseData, field.getDataObjectMethodName(), resultSet, tableName, field.getDbName());
                    break;
                case time:
                    getTimeField(baseData, field.getDataObjectMethodName(), resultSet, tableName, field.getDbName());
                    break;
                case dateTime:
                case dateTimeNow:
                    getDateTimeField(baseData, field.getDataObjectMethodName(), resultSet, tableName, field.getDbName());
                    break;
                case bytes:
                    getBytesField(baseData, field.getDataObjectMethodName(), resultSet, tableName, field.getDbName());
                    break;
                case enumerator:
                    getEnumeratorField(baseData, field.getDataObjectMethodName(), field.getEnumClass(), resultSet, tableName, field.getDbName());
                case password:
                    break;
            }
        }
    }

    protected VIEW fetchView(ResultSet resultSet) throws SQLException {
        return fetchView(getPrimaryTableName(), resultSet);
    }

    protected VIEW fetchView(String tableName, ResultSet resultSet) throws SQLException {
        VIEW view = newView(fetchData(tableName, resultSet));

        for (ForeignTableReference foreignTableReference : getForeignTableReferences()) {
            setForeignObjectInView(view, foreignTableReference.getDataObjectMethodName(), foreignTableReference.getForeignDbManager(),
                    foreignTableReference.getForeignTableAlias(), foreignTableReference.foreignEntityView, tableName, resultSet);
        }

        return view;
    }

    protected int fillStatementFromView(PreparedStatement statement, VIEW view, boolean create) throws SQLException {
        List<Field> fields = getFields()
                .stream()
                .filter(create ? f -> f.isOnCreate() : f -> f.isOnUpdate())
                .collect(Collectors.toList());

        int iField = 0;
        for (Field field : fields) {
            switch (field.getType()) {
                case number:
                    setNumberField(statement, ++iField, view, field.getDataObjectMethodName());
                    break;
                case text:
                    setTextField(statement, ++iField, view, field.getDataObjectMethodName());
                    break;
                case bool:
                    setBoolField(statement, ++iField, view, field.getDataObjectMethodName());
                    break;
                case date:
                    setDateField(statement, ++iField, view, field.getDataObjectMethodName());
                    break;
                case time:
                    setTimeField(statement, ++iField, view, field.getDataObjectMethodName());
                    break;
                case dateTime:
                    setDateTimeField(statement, ++iField, view, field.getDataObjectMethodName());
                    break;
                case dateTimeNow:
                    setDateTimeNowField(statement, ++iField);
                    break;
                case bytes:
                    setBytesField(statement, ++iField, view, field.getDataObjectMethodName());
                    break;
                case enumerator:
                    setEnumeratorField(statement, ++iField, view, field.getDataObjectMethodName());
                    break;
                case password:
                    setPasswordField(statement, ++iField, view, field.getDataObjectMethodName());
                    break;
                case loggedInUser:
                    setLoggedInUserField(statement, ++iField);
                    break;
            }
        }

        for (ForeignTableReference foreignTableReference : getForeignTableReferences()) {
            setForeignKeyField(statement, ++iField, view, foreignTableReference.getDataObjectMethodName());
        }
        return iField;
    }

    protected void getNumberField(DATA baseData, String dataObjectMethodName, ResultSet resultSet, String dbTableName, String dbFieldName) {
        try {
            Integer dbValue = (Integer)resultSet.getObject(dbTableName + "." + dbFieldName);
            String methodName = SETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName, Integer.class);
            method.invoke(baseData, dbValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void getTextField(DATA baseData, String dataObjectMethodName, ResultSet resultSet, String dbTableName, String dbFieldName) {
        try {
            String dbValue = resultSet.getString(dbTableName + "." + dbFieldName);
            String methodName = SETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName, String.class);
            method.invoke(baseData, dbValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void getBoolField(DATA baseData, String dataObjectMethodName, ResultSet resultSet, String dbTableName, String dbFieldName) {
        try {
            Boolean dbValue = resultSet.getBoolean(dbTableName + "." + dbFieldName);
            String methodName = SETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName, boolean.class);
            method.invoke(baseData, dbValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void getDateField(DATA baseData, String dataObjectMethodName, ResultSet resultSet, String dbTableName, String dbFieldName) {
        try {
            java.sql.Date sqlDate = resultSet.getDate(dbTableName + "." + dbFieldName);
            Date dbValue = sqlDate == null ? null : new Date(sqlDate.getTime());
            String methodName = SETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName, Date.class);
            method.invoke(baseData, dbValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void getTimeField(DATA baseData, String dataObjectMethodName, ResultSet resultSet, String dbTableName, String dbFieldName) {
        try {
            Time time = resultSet.getTime(dbTableName + "." + dbFieldName);
            Date dbValue = time == null ? null : new Date(time.getTime());
            String methodName = SETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName, Date.class);
            method.invoke(baseData, dbValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void getDateTimeField(DATA baseData, String dataObjectMethodName, ResultSet resultSet, String dbTableName, String dbFieldName) {
        try {
            Timestamp timeStamp = resultSet.getTimestamp(dbTableName + "." + dbFieldName);
            Date dbValue = timeStamp == null ? null : new Date(timeStamp.getTime());
            String methodName = SETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName, Date.class);
            method.invoke(baseData, dbValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void getBytesField(DATA baseData, String dataObjectMethodName, ResultSet resultSet, String dbTableName, String dbFieldName) {
        try {
            byte[] dbValue = resultSet.getBytes(dbTableName + "." + dbFieldName);
            String methodName = SETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName, byte[].class);
            method.invoke(baseData, dbValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void getEnumeratorField(DATA baseData, String dataObjectMethodName, Class<? extends BaseEnum> enumClass, ResultSet resultSet, String dbTableName, String dbFieldName) {
        try {
            Integer numberValue = (Integer)resultSet.getObject(dbTableName + "." + dbFieldName);
            Class.forName(enumClass.getName());
            BaseEnum enumValue = numberValue == null ? null : EnumUtil.get().getByClassAndNumberValue(enumClass, numberValue);
            String methodName = SETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName, enumClass);
            method.invoke(baseData, enumValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setNumberField(PreparedStatement statement, int iField, DATA baseData, String dataObjectMethodName) {
        try {
            String methodName = GETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName);
            Integer dataObjectFieldValue = (Integer) method.invoke(baseData);
            statement.setObject(iField, dataObjectFieldValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setTextField(PreparedStatement statement, int iField, DATA baseData, String dataObjectMethodName) {
        try {
            String methodName = GETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName);
            String dataObjectFieldValue = (String) method.invoke(baseData);
            statement.setString(iField, dataObjectFieldValue);
        }
        catch (Exception exception) {
                throw new UncheckedWrapperException(exception);
        }
    }

    protected void setBoolField(PreparedStatement statement, int iField, DATA baseData, String dataObjectMethodName) {
        try {
            String methodName = IS_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName);
            Boolean dataObjectFieldValue = (Boolean) method.invoke(baseData);
            statement.setBoolean(iField, dataObjectFieldValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setDateField(PreparedStatement statement, int iField, DATA baseData, String dataObjectMethodName) {
        try {
            String methodName = GETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName);
            Date dataObjectFieldValue = (Date) method.invoke(baseData);
            statement.setDate(iField, dataObjectFieldValue == null ? null : new java.sql.Date(dataObjectFieldValue.getTime()));

        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setTimeField(PreparedStatement statement, int iField, DATA baseData, String dataObjectMethodName) {
        try {
            String methodName = GETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName);
            Date dataObjectFieldValue = (Date) method.invoke(baseData);
            statement.setTime(iField, dataObjectFieldValue == null ? null : new Time(dataObjectFieldValue.getTime()));

        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setDateTimeField(PreparedStatement statement, int iField, DATA baseData, String dataObjectMethodName) {
        try {
            String methodName = GETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName);
            Date dataObjectFieldValue = (Date) method.invoke(baseData);
            statement.setTimestamp(iField, dataObjectFieldValue == null ? null : new Timestamp(dataObjectFieldValue.getTime()));
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setDateTimeNowField(PreparedStatement statement, int iField) {
        try {
            Timestamp now = new Timestamp(new Date().getTime());
            statement.setTimestamp(iField, now);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setBytesField(PreparedStatement statement, int iField, DATA baseData, String dataObjectMethodName) {
        try {
            String methodName = GETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName);
            byte[] dataObjectFieldValue = (byte[]) method.invoke(baseData);
            statement.setBytes(iField, dataObjectFieldValue);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setEnumeratorField(PreparedStatement statement, int iField, DATA baseData, String dataObjectMethodName) {
        try {
            String methodName = GETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName);
            BaseEnum dataObjectFieldValue = (BaseEnum) method.invoke(baseData);
            statement.setObject(iField, dataObjectFieldValue == null ? null : dataObjectFieldValue.getNumberValue());
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setPasswordField(PreparedStatement statement, int iField, DATA baseData, String dataObjectMethodName) {
        try {
            String methodName = GETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = baseData.getClass().getMethod(methodName);
            String dataObjectFieldValue = (String) method.invoke(baseData);
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            statement.setString(iField, passwordEncoder.encode(dataObjectFieldValue));
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setLoggedInUserField(PreparedStatement statement, int iField) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            statement.setString(iField, authentication.getName());
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setForeignKeyField(PreparedStatement statement, int iField, DATA baseData, String dataObjectForeignKeyMethod) {
        try {
            String methodName = GETTER_METHOD_PREFIX + dataObjectForeignKeyMethod;
            Method method = baseData.getClass().getMethod(methodName);
            BaseData foreignDataObject = (BaseData) method.invoke(baseData);
            statement.setObject(iField, foreignDataObject == null ? null : foreignDataObject.getId());
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setForeignObjectInView(VIEW view, String dataObjectMethodName, BaseDbManager foreignDbManager, boolean foreignEntityView, Integer foreignKey) {
        try {
            if (foreignKey == null) {
                return;
            }
            BaseData foreignObject = foreignEntityView ? foreignDbManager.getViewById(foreignKey) : foreignDbManager.getDataById(foreignKey);
            String methodName = SETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = view.getClass().getMethod(methodName, foreignObject.getClass());
            method.invoke(view, foreignObject);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected void setForeignObjectInView(VIEW view, String dataObjectMethodName, BaseDbManager foreignDbManager, String foreignTableAlias, boolean foreignEntityView, String tableName, ResultSet resultSet) {
        try {
            BaseData foreignObject = foreignEntityView ?
                    foreignDbManager.fetchView(calcForeignTableAlias(tableName, foreignTableAlias), resultSet) :
                    foreignDbManager.fetchData(calcForeignTableAlias(tableName, foreignTableAlias), resultSet);
            String methodName = SETTER_METHOD_PREFIX + dataObjectMethodName;
            Method method = view.getClass().getMethod(methodName, foreignObject.getClass());
            if (foreignObject.getId() == null) {
                foreignObject = null;
            }
            method.invoke(view, foreignObject);
        }
        catch (Exception exception) {
            throw new UncheckedWrapperException(exception);
        }
    }

    protected abstract String getPrimaryTableName();

    protected String getDefaultForeignKey() {
        throw new UnsupportedOperationException("getDefaultForeignKey is not supportted for this class. Pls override and implement getDefaultForeignKey method");
    }

    protected String getDefaultForeignMethodName() {
        throw new UnsupportedOperationException("getDefaultForeignMethodName is not supportted for this class. Pls override and implement getDefaultForeignMethodName method");
    }

    protected List<Field> getFields() {
        List<Field> fields = new ArrayList<Field>();
        fields.add(new Field(DB_FIELD_ID, "Id", FieldType.number, false, false));
        return fields;
    }

    protected List<String> calcDbFields(boolean create) {
        List<String> dbFields = getFields().stream()
                .filter(create ? f -> f.isOnCreate() : f -> f.isOnUpdate())
                .map(f -> f.getDbName())
                .collect(Collectors.toList());
        List<String> foreignKeyFields = getForeignTableReferences().stream()
                .map(ftr -> ftr.getReferrerTableForeignKey())
                .collect(Collectors.toList());

        dbFields.addAll(foreignKeyFields);
        return dbFields;
    }


    protected List<ForeignTableReference> getForeignTableReferences() {
        return getForeignTableReferences(getPrimaryTableName());
    }

    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        return Collections.emptyList();
    }

    protected List<ForeignTableReference> getForeignTableReferencesIncludingIndirectReferences() {
        return getForeignTableReferencesIncludingIndirectReferences(getPrimaryTableName());
    }

    protected List<ForeignTableReference> getForeignTableReferencesIncludingIndirectReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferencesWithIndirectReferences = new ArrayList<ForeignTableReference>();
        for (ForeignTableReference foreignTableReference : getForeignTableReferences(tableName)) {
            foreignTableReferencesWithIndirectReferences.add(foreignTableReference);
            if (foreignTableReference.isForeignEntityView()) {
                foreignTableReferencesWithIndirectReferences.addAll(
                        foreignTableReference.getForeignDbManager().getForeignTableReferencesIncludingIndirectReferences(
                                calcForeignTableAlias(tableName, foreignTableReference.getForeignTableAlias())));
            }
        }
        return foreignTableReferencesWithIndirectReferences;
    }

    protected static String calcForeignTableAlias(String referrerTable, String foreignTable) {
        return referrerTable + DB_FIELD_NAME_SEPARATOR + foreignTable;
    }

    protected static class Field {
        private String dbName;
        private String dataObjectMethodName;
        private FieldType type;
        private Class<? extends BaseEnum> enumClass;
        private boolean onCreate;
        private boolean onUpdate;

        public Field(String dbName, String dataObjectMethodName, FieldType type) {
            this (dbName, dataObjectMethodName, type, null,true, true);
        }

        public Field(String dbName, String dataObjectMethodName, FieldType type, boolean onCreate, boolean onUpdate) {
            this (dbName, dataObjectMethodName, type, null, onCreate, onUpdate);
        }

        public Field(String dbName, String dataObjectMethodName, Class<? extends BaseEnum> enumClass) {
            this (dbName, dataObjectMethodName, FieldType.enumerator, enumClass, true, true);
        }

        public Field(String dbName, String dataObjectMethodName, Class<? extends BaseEnum> enumClass, boolean onCreate, boolean onUpdate) {
            this (dbName, dataObjectMethodName, FieldType.enumerator, enumClass, onCreate, onUpdate);
        }

        private Field(String dbName, String dataObjectMethodName, FieldType type, Class<? extends BaseEnum> enumClass, boolean onCreate, boolean onUpdate) {
            this.dbName = dbName;
            this.dataObjectMethodName = dataObjectMethodName;
            this.type = type;
            this.enumClass = enumClass;
            this.onCreate = onCreate;
            this.onUpdate = onUpdate;
        }

        public String getDbName() {
            return dbName;
        }

        public String getDataObjectMethodName() {
            return dataObjectMethodName;
        }

        public FieldType getType() {
            return type;
        }

        public boolean isOnCreate() {
            return onCreate;
        }

        public boolean isOnUpdate() {
            return onUpdate;
        }

        public Class<? extends BaseEnum> getEnumClass() {
            return enumClass;
        }
    }

    protected enum FieldType {
        number,
        text,
        bool,
        date,
        time,
        dateTime,
        dateTimeNow,
        bytes,
        enumerator,
        password,
        loggedInUser;
    }

    protected static class ForeignTableReference {
        private String referrerTable;
        private String referrerTableForeignKey;
        private String foreignTable;
        private String foreignTableKey;
        private String foreignTableAlias;
        boolean nullPermitted;
        private String dataObjectMethodName;
        private BaseDbManager foreignDbManager;
        private boolean foreignEntityView;

        public ForeignTableReference(String referrerTable, BaseDbManager foreignDbManager, boolean foreignEntityView) {
            this(referrerTable, foreignDbManager.getDefaultForeignKey(), foreignDbManager.getPrimaryTableName(), DB_FIELD_ID,
                    null, false, foreignDbManager.getDefaultForeignMethodName(), foreignDbManager, foreignEntityView);
        }

        public ForeignTableReference(String referrerTable, String referrerTableForeignKey, String foreignTable, String dataObjectMethodName, BaseDbManager foreignDbManager, boolean foreignEntityView) {
            this(referrerTable, referrerTableForeignKey, foreignTable, DB_FIELD_ID, null, false, dataObjectMethodName, foreignDbManager, foreignEntityView);
        }

        public ForeignTableReference(String referrerTable, String referrerTableForeignKey, String foreignTable, String foreignTableKey, String foreignTableAlias,
                                     boolean nullPermitted, String dataObjectMethodName, BaseDbManager foreignDbManager, boolean foreignEntityView) {
            this.referrerTable = referrerTable;
            this.referrerTableForeignKey = referrerTableForeignKey;
            this.foreignTable = foreignTable;
            this.foreignTableKey = foreignTableKey;
            this.foreignTableAlias = foreignTableAlias == null ? foreignTable : foreignTableAlias;
            this.nullPermitted = nullPermitted;
            this.dataObjectMethodName = dataObjectMethodName;
            this.foreignDbManager = foreignDbManager;
            this.foreignEntityView = foreignEntityView;
        }

        public String getReferrerTable() {
            return referrerTable;
        }

        public String getReferrerTableForeignKey() {
            return referrerTableForeignKey;
        }

        public String getForeignTable() {
            return foreignTable;
        }

        public String getForeignTableKey() {
            return foreignTableKey;
        }

        public String getForeignTableAlias() {
            return foreignTableAlias;
        }

        public boolean isNullPermitted() {
            return nullPermitted;
        }

        public String getDataObjectMethodName() {
            return dataObjectMethodName;
        }

        public BaseDbManager getForeignDbManager() {
            return foreignDbManager;
        }

        public boolean isForeignEntityView() {
            return foreignEntityView;
        }
    }
}
