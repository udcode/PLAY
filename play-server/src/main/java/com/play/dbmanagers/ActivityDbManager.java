package com.play.dbmanagers;

import com.play.data.ActivityData;
import com.play.view.ActivityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class ActivityDbManager extends BaseEntityDbManager<ActivityData, ActivityView> {
    @Autowired
    private ActivityTypeDbManager activityTypeDbManager;

    public Collection<ActivityData> findDatasByActivityTypeId(int activityTypeId) {
        return findDatasByIntField(activityTypeDbManager.getDefaultForeignKey(), activityTypeId);
    }

    public ActivityView createActivity(ActivityData activity, int activityTypeId) {
        return create(activity, activityTypeId);
    }

    public ActivityView updateActivity(ActivityData activity, int activityTypeId) {
        return update(activity, activityTypeId);
    }

    @Override
    protected ActivityData newData() {
        return new ActivityData();
    }

    @Override
    protected ActivityView newView(ActivityData activity) {
        return new ActivityView(activity);
    }

    @Override
    protected String getPrimaryTableName() {
        return "activities";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "activity_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "Activity";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("title", "Title", FieldType.text));
        fields.add(new Field("description", "Description", FieldType.text));
        fields.add(new Field("picture", "Picture", FieldType.bytes));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, activityTypeDbManager,  false));
        return foreignTableReferences;
    }
}
