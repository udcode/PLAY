package com.play.dbmanagers;

import com.play.data.*;
import com.play.view.UserView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class UserDbManager extends BaseEntityDbManager<UserData, UserView> {
    private static final String REFERENCE_USER_FOREIGN_TABLE_NAME = "reference_users";
    private static final String REFERENCE_USER_PRIMARY_KEY = "reference_user_id";

    @Autowired
    private UserRoleDbManager userRoleDbManager;

    public Collection<UserView> findViewsByReferenceUserId(int referenceUserId) {
        return findViewsByIntField(REFERENCE_USER_PRIMARY_KEY, referenceUserId);
    }

    public UserView createUser(UserData user, Integer referenceUserId, Collection<UserRoleEnum> roles) {
        if (roles == null || roles.isEmpty()) {
            throw new IllegalArgumentException("Cannot create user without roles");
        }

        UserView userView = create(user, referenceUserId);

        Collection<UserRoleData> userRoleDatas = handleUserRoles(userView, roles, true);
        userView.setRoles(userRoleDatas);
        return userView;
    }

    public UserView updateUser(UserData user, Integer referenceUserId, Collection<UserRoleEnum> roles) {
        UserView userView = update(user, referenceUserId);

        if (roles == null || roles.isEmpty()) {
            return userView;
        }

        Collection<UserRoleData> userRoleDatas = handleUserRoles(userView, roles, false);
        userView.setRoles(userRoleDatas);
        return userView;
    }

    private Collection<UserRoleData> handleUserRoles(UserView user, Collection<UserRoleEnum> roles, boolean create) {
        if (!create) {
            userRoleDbManager.deleteByIntField(getDefaultForeignKey(), user.getId());
        }
        Collection<UserRoleData> userRoleDatas = new ArrayList<UserRoleData>();
        for (UserRoleEnum role : roles) {
            UserRoleData userRoleBeforeCreation = new UserRoleData(null, role);
            UserRoleData createdUserRole = new UserRoleData (userRoleDbManager.createUserRole(userRoleBeforeCreation, user.getId()));
            userRoleDatas.add(createdUserRole);
        }
        return userRoleDatas;
    }

    @Override
    protected UserData newData() {
        return new UserData();
    }

    @Override
    protected UserView newView(UserData user) {
        return new UserView(user);
    }

    @Override
    protected UserView fetchView (String tableName, ResultSet resultSet) throws SQLException {
        UserView user = super.fetchView(tableName, resultSet);
        user.setRoles(userRoleDbManager.findDatasByUserId(user.getId()));
        return user;
    }

    @Override
    protected String getPrimaryTableName() {
        return "users";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "user_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "User";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("first_name", "FirstName", FieldType.text));
        fields.add(new Field("last_name", "LastName", FieldType.text));
        fields.add(new Field("phone_number", "PhoneNumber", FieldType.text));
        fields.add(new Field("email", "Email", FieldType.text));
        fields.add(new Field("picture", "Picture", FieldType.bytes));
        fields.add(new Field("birth_date", "BirthDate", FieldType.date));
        fields.add(new Field("gender", "Gender", GenderEnum.class));
        fields.add(new Field("address", "Address", FieldType.text));
        fields.add(new Field("city", "City", FieldType.text));
        fields.add(new Field("state", "State", FieldType.text));
        fields.add(new Field("country", "Country", FieldType.text));
        fields.add(new Field("password", "Password", FieldType.password, true, false));
        fields.add(new Field("reference_type", "ReferenceType", ReferenceTypeEnum.class));
        fields.add(new Field("reference_details", "ReferenceDetails", FieldType.text));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, REFERENCE_USER_PRIMARY_KEY, getPrimaryTableName(), DB_FIELD_ID,
                REFERENCE_USER_FOREIGN_TABLE_NAME, true, "ReferenceUser", this, false));
        return foreignTableReferences;
    }
}