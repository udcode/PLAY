package com.play.dbmanagers;

import com.play.data.StaffMemberData;
import com.play.data.StaffMemberPermissionsEnum;
import com.play.view.StaffMemberView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StaffMemberDbManager extends BaseEntityDbManager<StaffMemberData, StaffMemberView> {
    @Autowired
    private UserDbManager userDbManager;

    public StaffMemberView getViewByUserId(int userId) {
        return getViewByIntField(userDbManager.getDefaultForeignKey(), userId);
    }

    public StaffMemberView createStaffMember(StaffMemberData staffMember, int userId) {
        return create(staffMember, userId);
    }

    public StaffMemberView updateStaffMember(StaffMemberData staffMember, int userId) {
        return update(staffMember, userId);
    }

    @Override
    protected StaffMemberData newData() {
        return new StaffMemberData();
    }

    @Override
    protected StaffMemberView newView(StaffMemberData staffMember) {
        return new StaffMemberView(staffMember);
    }

    @Override
    protected String getPrimaryTableName() {
        return "staff_members";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "staff_member_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "StaffMember";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("permissions", "Permissions", StaffMemberPermissionsEnum.class));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, userDbManager,false));
        return foreignTableReferences;
    }
}