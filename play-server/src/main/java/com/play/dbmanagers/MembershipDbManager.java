package com.play.dbmanagers;

import com.play.data.CurrencyEnum;
import com.play.data.MembershipData;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MembershipDbManager extends BaseEntityDbManager<MembershipData, MembershipData> {
    public MembershipData createMembership(MembershipData membership) {
        return create(membership);
    }

    public MembershipData updateMembership(MembershipData membership) {
        return update(membership);
    }

    @Override
    protected MembershipData newData() {
        return new MembershipData();
    }

    @Override
    protected MembershipData newView(MembershipData membership) {
        return membership;
    }

    @Override
    protected String getPrimaryTableName() {
        return "memberships";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "membership_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "Membership";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("membership_name", "MembershipName", FieldType.text));
        fields.add(new Field("price", "Price", FieldType.number));
        fields.add(new Field("currency", "Currency", CurrencyEnum.class));
        fields.add(new Field("membership_level", "MembershipLevel", FieldType.number));
        fields.add(new Field("max_sessions_number", "MaxSessionsNumber", FieldType.number));
        fields.add(new Field("max_sessions_period", "MaxSessionsPeriod", FieldType.number));
        return fields;
    }
}
