package com.play.dbmanagers;

import com.play.data.UserRoleData;
import com.play.data.UserRoleEnum;
import com.play.view.UserRoleView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class UserRoleDbManager extends BaseDbManager<UserRoleData, UserRoleView> {

    @Autowired
    private UserDbManager userDbManager;

    public Collection<UserRoleData> findDatasByUserId(int userId) {
        return findDatasByIntField(userDbManager.getDefaultForeignKey(), userId);
    }

    public UserRoleData createUserRole(UserRoleData userRole, int userId) {
        return create(userRole, userId);
    }

    @Override
    protected UserRoleData newData() {
        return new UserRoleData();
    }

    @Override
    protected UserRoleView newView(UserRoleData userRole) {
        return new UserRoleView(userRole);
    }

    @Override
    protected String getPrimaryTableName() {
        return "users_roles";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("role", "Role", UserRoleEnum.class));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, userDbManager, false));
        return foreignTableReferences;
    }
}