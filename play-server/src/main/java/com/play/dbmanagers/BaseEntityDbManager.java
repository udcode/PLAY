package com.play.dbmanagers;

import com.play.data.BaseEntityData;

import java.util.List;

public abstract class BaseEntityDbManager<ENTITY extends BaseEntityData, ENTITYVIEW extends ENTITY> extends BaseDbManager<ENTITY, ENTITYVIEW> {

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("creation_time", "CreationTime", FieldType.dateTimeNow, true, false));
        fields.add(new Field("created_by", "CreatedBy", FieldType.loggedInUser, true, false));
        fields.add(new Field("last_modification_time", "LastModificationTime", FieldType.dateTimeNow));
        fields.add(new Field("last_modified_by", "LastModifiedBy", FieldType.loggedInUser));
        fields.add(new Field("active", "Active", FieldType.bool));
        return fields;
    }
}
