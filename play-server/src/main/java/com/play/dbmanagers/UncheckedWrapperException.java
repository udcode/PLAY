package com.play.dbmanagers;

public class UncheckedWrapperException extends RuntimeException {
    public UncheckedWrapperException(Exception execption) {
        super(execption);
    }
}
