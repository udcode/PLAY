package com.play.dbmanagers;

import com.play.data.ActivityTypeData;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ActivityTypeDbManager extends BaseEntityDbManager<ActivityTypeData, ActivityTypeData> {
    public ActivityTypeData createActivityType(ActivityTypeData activityType) {
        return create(activityType);
    }

    public ActivityTypeData updateActivityType(ActivityTypeData activityType) {
        return update(activityType);
    }

    @Override
    protected ActivityTypeData newData() {
        return new ActivityTypeData();
    }

    @Override
    protected ActivityTypeData newView(ActivityTypeData activityType) {
        return activityType;
    }

    @Override
    protected String getPrimaryTableName() {
        return "activity_types";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "activity_type_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "ActivityType";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("title", "Title", FieldType.text));
        fields.add(new Field("description", "Description", FieldType.text));
        fields.add(new Field("picture", "Picture", FieldType.bytes));
        return fields;
    }
}