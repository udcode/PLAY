package com.play.dbmanagers;

import com.play.data.InstructorData;
import com.play.view.InstructorView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InstructorDbManager extends BaseEntityDbManager<InstructorData, InstructorView>{
    @Autowired
    private UserDbManager userDbManager;

    public InstructorView getViewByUserId(int userId) {
        return getViewByIntField(userDbManager.getDefaultForeignKey(), userId);
    }

    public InstructorView createInstructor(InstructorData instructor, int userId) {
        return create(instructor, userId);
    }

    public InstructorView updateInstructor(InstructorData instructor, int userId) {
        return update(instructor, userId);
    }

    @Override
    protected InstructorData newData() {
        return new InstructorData();
    }

    @Override
    protected InstructorView newView(InstructorData instructor) {
        return new InstructorView(instructor);
    }

    @Override
    protected String getPrimaryTableName() {
        return "instructors";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "instructor_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "Instructor";
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, userDbManager,  false));
        return foreignTableReferences;
    }
}
