package com.play.dbmanagers;

import com.play.data.UserEventData;
import com.play.data.UserEventTypeEnum;
import com.play.view.UserEventView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class UserEventDbManager extends BaseEntityDbManager<UserEventData, UserEventView> {
    @Autowired
    private UserDbManager userDbManager;
    @Autowired
    private StaffMemberDbManager staffMemberDbManager;

    public Collection<UserEventView> findViewsByUserId(int userId) {
        return findViewsByIntField(userDbManager.getDefaultForeignKey(), userId);
    }

    public Collection<UserEventView> findViewsByStaffMemberId(int staffMemberId) {
        return findViewsByIntField(staffMemberDbManager.getDefaultForeignKey(), staffMemberId);
    }

    public UserEventView createUserEvent(UserEventData userEvent, int userId, int staffMemberId) {
        return create(userEvent, userId, staffMemberId);
    }

    public UserEventView updateUserEvent(UserEventData userEvent, int userId, int staffMemberId) {
        return update(userEvent, userId, staffMemberId);
    }

    @Override
    protected UserEventData newData() {
        return new UserEventData();
    }

    @Override
    protected UserEventView newView(UserEventData userEvent) {
        return new UserEventView(userEvent);
    }

    @Override
    protected String getPrimaryTableName() {
        return "user_events";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "user_event_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "UserEvent";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("event_type", "EventType", UserEventTypeEnum.class));
        //fields.add(new Field("sub_event", "SubEvent", SomeEnumClass, false, false));
        fields.add(new Field("details", "Details", FieldType.text));
        fields.add(new Field("follow_up_time", "FollowUpTime", FieldType.dateTime));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, userDbManager, false));
        foreignTableReferences.add(new ForeignTableReference(tableName, staffMemberDbManager, true));
        return foreignTableReferences;
    }
}