package com.play.dbmanagers;

import com.play.data.DayOfWeekEnum;
import com.play.data.SessionDefinitionScheduleData;
import com.play.view.SessionDefinitionScheduleView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class SessionDefinitionScheduleDbManager extends BaseEntityDbManager<SessionDefinitionScheduleData, SessionDefinitionScheduleView> {
    @Autowired
    private SessionDefinitionDbManager sessionDefinitionDbManager;

    public Collection<SessionDefinitionScheduleData> findDatasBySessionDefinitionId(int sessionDefinitionId) {
        return findDatasByIntField(sessionDefinitionDbManager.getDefaultForeignKey(), sessionDefinitionId);
    }

    public SessionDefinitionScheduleView createSessionDefinitionSchedule(SessionDefinitionScheduleData sessionDefinitionSchedule, int sessionDefinitionId) {
        return create(sessionDefinitionSchedule, sessionDefinitionId);
    }

    public SessionDefinitionScheduleView updateSessionDefinitionSchedule(SessionDefinitionScheduleData sessionDefinitionSchedule, int sessionDefinitionId) {
        return update(sessionDefinitionSchedule, sessionDefinitionId);
    }

    @Override
    protected SessionDefinitionScheduleData newData() {
        return new SessionDefinitionScheduleData();
    }

    @Override
    protected SessionDefinitionScheduleView newView(SessionDefinitionScheduleData sessionDefinitionSchedule) {
        return new SessionDefinitionScheduleView(sessionDefinitionSchedule);
    }

    @Override
    protected String getPrimaryTableName() {
        return "session_definition_schedules";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("day_of_week", "DayOfWeek", DayOfWeekEnum.class));
        fields.add(new Field("start_date", "StartDate", FieldType.date));
        fields.add(new Field("end_date", "EndDate", FieldType.date));
        fields.add(new Field("start_time", "StartTime", FieldType.time));
        fields.add(new Field("end_time", "EndTime", FieldType.time));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, sessionDefinitionDbManager, false));
        return foreignTableReferences;
    }
}
