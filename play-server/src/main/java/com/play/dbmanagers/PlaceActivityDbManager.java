package com.play.dbmanagers;

import com.play.data.PlaceActivityData;
import com.play.view.PlaceActivityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class PlaceActivityDbManager extends BaseDbManager<PlaceActivityData, PlaceActivityView> {
    @Autowired
    private PlaceDbManager placeDbManager;
    @Autowired
    private ActivityDbManager activityDbManager;

    public Collection<PlaceActivityView> findViewsByPlaceId(int placeId) {
        return findViewsByIntField(placeDbManager.getDefaultForeignKey(), placeId);
    }

    public Collection<PlaceActivityView> findViewsByActivityId(int activityId) {
        return findViewsByIntField(activityDbManager.getDefaultForeignKey(), activityId);
    }

    public PlaceActivityView createPlaceActivity(PlaceActivityData placeActivity, int placeId, int activityId) {
        return create(placeActivity, placeId, activityId);
    }

    public PlaceActivityView updatePlaceActivity(PlaceActivityData placeActivity, int placeId, int activityId) {
        return update(placeActivity, placeId, activityId);
    }

    @Override
    protected PlaceActivityData newData() {
        return new PlaceActivityData();
    }

    @Override
    protected PlaceActivityView newView(PlaceActivityData placeActivity) {
        return new PlaceActivityView(placeActivity);
    }

    @Override
    protected String getPrimaryTableName() {
        return "places_activities";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("min_players", "MinPlayers", FieldType.number));
        fields.add(new Field("max_players", "MaxPlayers", FieldType.number));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, placeDbManager, true));
        foreignTableReferences.add(new ForeignTableReference(tableName, activityDbManager, true));
        return foreignTableReferences;
    }
}