package com.play.dbmanagers;

import com.play.data.*;
import com.play.view.SessionDefinitionView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class SessionDefinitionDbManager extends BaseEntityDbManager<SessionDefinitionData, SessionDefinitionView> {
    @Autowired
    private InstructorDbManager instructorDbManager;
    @Autowired
    private PlaceDbManager placeDbManager;
    @Autowired
    private MembershipDbManager membershipDbManager;
    @Autowired
    private ActivityDbManager activityDbManager;
    @Autowired
    private SessionDefinitionScheduleDbManager sessionDefinitionScheduleDbManager;

    public Collection<SessionDefinitionView> findViewsByInstructorId(int instructorId) {
        return findViewsByIntField(instructorDbManager.getDefaultForeignKey(), instructorId);
    }

    public Collection<SessionDefinitionView> findViewsByPlaceId(int placeId) {
        return findViewsByIntField(placeDbManager.getDefaultForeignKey(), placeId);
    }

    public Collection<SessionDefinitionView> findViewsByMembershipId(int membershipId) {
        return findViewsByIntField(membershipDbManager.getDefaultForeignKey(), membershipId);
    }

    public Collection<SessionDefinitionView> findViewsByActivityId(int activityId) {
        return findViewsByIntField(activityDbManager.getDefaultForeignKey(), activityId);
    }

    public SessionDefinitionView createSessionDefinition(SessionDefinitionData sessionDefinition, int instructorId, int placeId, int membershipId, int activityId) {
        return create(sessionDefinition, instructorId, placeId, membershipId, activityId);
    }

    public SessionDefinitionView updateSessionDefinition(SessionDefinitionData sessionDefinition, int instructorId, int placeId, int membershipId, int activityId) {
        return update(sessionDefinition, instructorId, placeId, membershipId, activityId);
    }

    @Override
    protected SessionDefinitionData newData() {
        return new SessionDefinitionData();
    }

    @Override
    protected SessionDefinitionView newView(SessionDefinitionData sessionDefinition) {
        return new SessionDefinitionView(sessionDefinition);
    }

    @Override
    protected SessionDefinitionView fetchView (String tableName, ResultSet resultSet) throws SQLException {
        SessionDefinitionView sessionDefinition = super.fetchView(tableName, resultSet);
        sessionDefinition.setSchedules(sessionDefinitionScheduleDbManager.findDatasBySessionDefinitionId(sessionDefinition.getId()));
        return sessionDefinition;
    }

    @Override
    protected String getPrimaryTableName() {
        return "session_definitions";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "session_definition_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "SessionDefinition";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("title", "Title", FieldType.text));
        fields.add(new Field("min_players", "MinPlayers", FieldType.number));
        fields.add(new Field("max_players", "MaxPlayers", FieldType.number));
        fields.add(new Field("activity_level", "ActivityLevel", FieldType.number));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, instructorDbManager, false));
        foreignTableReferences.add(new ForeignTableReference(tableName, placeDbManager, false));
        foreignTableReferences.add(new ForeignTableReference(tableName, membershipDbManager, false));
        foreignTableReferences.add(new ForeignTableReference(tableName, activityDbManager, false));
        return foreignTableReferences;
    }
}