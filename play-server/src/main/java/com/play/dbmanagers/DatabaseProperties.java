package com.play.dbmanagers;

import com.play.utils.ServerProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("databaseProperties")
public class DatabaseProperties {

    @Autowired
    private ServerProperties serverProperties;

    public String getDatabaseUrl() {
        return "jdbc:mysql://" +
                serverProperties.getProperty("database_host") +
                "/" +
                serverProperties.getProperty("database_schema") +
                "?autoReconnect=true&useSSL=false";
    }

    public String getDatabaseUser() {
        return serverProperties.getProperty("database_user");
    }

    public String getDatabasePassword() {
        return serverProperties.getProperty("database_password");
    }

    public String getDriverClassName() {
        return "com.mysql.jdbc.Driver";
    }
}
