package com.play.dbmanagers;

import com.play.data.PlaceData;
import com.play.view.PlaceOwnerView;
import com.play.view.PlaceView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class PlaceDbManager extends BaseEntityDbManager<PlaceData, PlaceView> {
    @Autowired
    private PlaceOwnerDbManager placeOwnerDbManager;

    public Collection<PlaceView> findViewsByPlaceOwnerId(int placeOwnerId) {
        return findViewsByIntField(placeOwnerDbManager.getDefaultForeignKey(), placeOwnerId);
    }

    public PlaceView createPlace(PlaceData place, int placeOwnerId) {
        return create(place, placeOwnerId);
    }

    public PlaceView updatePlace(PlaceData place, int placeOwnerId) {
        return update(place, placeOwnerId);
    }

    @Override
    protected PlaceData newData() {
        return new PlaceData();
    }

    @Override
    protected PlaceView newView(PlaceData placeData) {
        return new PlaceView(placeData);
    }

    @Override
    protected String getPrimaryTableName() {
        return "places";
    }

    @Override
    protected String getDefaultForeignKey() {
        return "place_id";
    }

    @Override
    protected String getDefaultForeignMethodName() {
        return "Place";
    }

    @Override
    protected List<Field> getFields() {
        List<Field> fields = super.getFields();
        fields.add(new Field("place_name", "PlaceName", FieldType.text));
        fields.add(new Field("country", "Country", FieldType.text));
        fields.add(new Field("city", "City", FieldType.text));
        fields.add(new Field("address", "Address", FieldType.text));
        fields.add(new Field("min_players", "MinPlayers", FieldType.number));
        fields.add(new Field("max_players", "MaxPlayers", FieldType.number));
        fields.add(new Field("latitude", "Latitude", FieldType.number));
        fields.add(new Field("longitude", "Longitude", FieldType.number));
        return fields;
    }

    @Override
    protected List<ForeignTableReference> getForeignTableReferences(String tableName) {
        List<ForeignTableReference> foreignTableReferences = new ArrayList<>();
        foreignTableReferences.add(new ForeignTableReference(tableName, placeOwnerDbManager, true));
        return foreignTableReferences;
    }
}