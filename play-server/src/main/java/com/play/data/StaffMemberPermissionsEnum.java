package com.play.data;

import com.play.utils.EnumUtil;

public enum StaffMemberPermissionsEnum implements BaseEnum {
    manager(1, "Manager"),
    office(2, "Office"),
    sales(3, "Sales");

    private int numberValue;
    private String displayValue;

    private StaffMemberPermissionsEnum(int numberValue, String displayValue) {
        this.numberValue = numberValue;
        this.displayValue = displayValue;
        EnumUtil.get().add(this);
    }

    @Override
    public int getNumberValue() {
        return numberValue;
    }

    @Override
    public String getDisplayValue() {
        return displayValue;
    }
}

