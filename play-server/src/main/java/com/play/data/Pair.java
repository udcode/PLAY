package com.play.data;

public class Pair<FIRST, SECOND> {
    private FIRST first;
    private SECOND second;

    public FIRST getFirst() {
        return first;
    }

    public SECOND getSecond() {
        return second;
    }

    public Pair(FIRST first, SECOND second) {
        this.first = first;
        this.second = second;
    }

    public String toString() {
        return first + "/" + second;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (first != null ? first.hashCode() : 0);
        hash = 31 * hash + (second != null ? second.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other instanceof Pair) {
            Pair otherPair = (Pair) other;
            if (first != null ? !first.equals(otherPair.first) : otherPair.first != null) {
                return false;
            }
            if (second != null ? !second.equals(otherPair.second) : otherPair.second != null) {
                return false;
            }
            return true;
        }
        return false;
    }

}
