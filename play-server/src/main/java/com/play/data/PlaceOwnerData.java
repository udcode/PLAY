package com.play.data;

public class PlaceOwnerData extends BaseEntityData {
    public PlaceOwnerData() {
    }

    public PlaceOwnerData(Integer id) {
        super(id);
    }

    public PlaceOwnerData(PlaceOwnerData placeOwner) {
        super(placeOwner);
    }
}
