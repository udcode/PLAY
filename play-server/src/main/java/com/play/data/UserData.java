package com.play.data;

import java.util.Date;

public class UserData extends BaseEntityData {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private byte[] picture;
    private Date birthDate;
    private GenderEnum gender;
    private String address;
    private String city;
    private String state;
    private String country;
    private String password;
    private ReferenceTypeEnum referenceType;
    private String referenceDetails;

    public UserData() {
    }

    public UserData(Integer id, String firstName, String lastName, String phoneNumber, String email, byte[] picture,
                    Date birthDate, GenderEnum gender, String address, String city, String state, String country, String password,
                    ReferenceTypeEnum referenceType, String referenceDetails) {
        super(id);
        init(firstName, lastName, phoneNumber, email, picture, birthDate, gender, address, city, state, country, password, referenceType, referenceDetails);
    }

    public UserData(UserData user) {
        super(user);
        init(user.getFirstName(), user.getLastName(), user.getPhoneNumber(), user.getEmail(), user.getPicture(), user.getBirthDate(),
                user.getGender(), user.getAddress(), user.getCity(), user.getState(), user.getCountry(), user.getPassword(),
                user.getReferenceType(), user.getReferenceDetails());
    }

    private void init(String firstName, String lastName, String phoneNumber, String email, byte[] picture, Date birthDate,
                      GenderEnum gender, String address, String city, String state, String country, String password,
                      ReferenceTypeEnum referenceType, String referenceDetails) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.picture = picture;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.city = city;
        this.state = state;
        this.country = country;
        this.password = password;
        this.referenceType = referenceType;
        this.referenceDetails = referenceDetails;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return  lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public GenderEnum getGender()  {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ReferenceTypeEnum getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(ReferenceTypeEnum referenceType) {
        this.referenceType = referenceType;
    }

    public String getReferenceDetails() {
        return referenceDetails;
    }

    public void setReferenceDetails(String referenceDetails) {
        this.referenceDetails = referenceDetails;
    }
}
