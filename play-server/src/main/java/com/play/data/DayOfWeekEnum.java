package com.play.data;

import com.play.utils.EnumUtil;

public enum DayOfWeekEnum implements BaseEnum {
    monday(1, "Monday"),
    tuesday(2, "Tuesday"),
    wednesday(3, "Wednesday"),
    thursday(4, "Thursday"),
    friday(5, "Friday"),
    saturday(6, "Saturday"),
    sunday(7, "Sunday");

    private int numberValue;
    private String displayValue;

    private DayOfWeekEnum(int numberValue, String displayValue) {
        this.numberValue = numberValue;
        this.displayValue = displayValue;
        EnumUtil.get().add(this);
    }

    @Override
    public int getNumberValue() {
        return numberValue;
    }

    @Override
    public String getDisplayValue() {
        return displayValue;
    }
}
