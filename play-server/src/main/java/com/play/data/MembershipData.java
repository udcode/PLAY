package com.play.data;

public class MembershipData extends BaseEntityData {
    private String membershipName;
    private Integer price;
    private CurrencyEnum currency;
    private Integer membershipLevel;
    private Integer maxSessionsNumber;
    private Integer maxSessionsPeriod;

    public MembershipData() {
    }

    public MembershipData(Integer id, String membershipName, Integer price, CurrencyEnum currency, Integer membershipLevel, Integer maxSessionsNumber, Integer maxSessionsPeriod) {
        super(id);
        init(membershipName, price, currency, membershipLevel, maxSessionsNumber, maxSessionsPeriod);
    }

    public MembershipData(MembershipData membership) {
        super(membership);
        init(membership.getMembershipName(), membership.getPrice(), membership.getCurrency(), membership.getMembershipLevel(),
                membership.getMaxSessionsNumber(), membership.getMaxSessionsPeriod());
    }

    private void init(String membershipName, Integer price, CurrencyEnum currency, Integer membershipLevel, Integer maxSessionsNumber, Integer maxSessionsPeriod) {
        this.membershipName = membershipName;
        this.price = price;
        this.currency = currency;
        this.membershipLevel = membershipLevel;
        this.maxSessionsNumber = maxSessionsNumber;
        this.maxSessionsPeriod = maxSessionsPeriod;
    }

    public String getMembershipName() {
        return membershipName;
    }

    public void setMembershipName(String membershipName) {
        this.membershipName = membershipName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public CurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyEnum currency) {
        this.currency = currency;
    }

    public Integer getMembershipLevel() {
        return membershipLevel;
    }

    public void setMembershipLevel(Integer membershipLevel) {
        this.membershipLevel = membershipLevel;
    }

    public Integer getMaxSessionsNumber() {
        return maxSessionsNumber;
    }

    public void setMaxSessionsNumber(Integer maxSessionsNumber) {
        this.maxSessionsNumber = maxSessionsNumber;
    }

    public Integer getMaxSessionsPeriod() {
        return maxSessionsPeriod;
    }

    public void setMaxSessionsPeriod(Integer maxSessionsPeriod) {
        this.maxSessionsPeriod = maxSessionsPeriod;
    }
}
