package com.play.data;

import com.play.utils.EnumUtil;

public enum UserRoleEnum implements BaseEnum{
    player(1, "Player"),
    instructor(2, "Instructor"),
    placeOwner(3, "Place Owner"),
    staffMember(4, "Staff Member"),
    administrator(5, "Administrator");

    private int numberValue;
    private String displayValue;

    private UserRoleEnum(int numberValue, String displayValue) {
        this.numberValue = numberValue;
        this.displayValue = displayValue;
        EnumUtil.get().add(this);
    }

    @Override
    public int getNumberValue() {
        return numberValue;
    }

    @Override
    public String getDisplayValue() {
        return displayValue;
    }
}
