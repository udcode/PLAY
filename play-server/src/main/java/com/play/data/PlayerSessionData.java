package com.play.data;

public class PlayerSessionData extends BaseData {
    private boolean playerArrived;

    public PlayerSessionData() {
    }

    public PlayerSessionData(Integer id, boolean playerArrived) {
        super(id);
        init(playerArrived);
    }

    public PlayerSessionData(PlayerSessionData playerSession) {
        super(playerSession);
        init(playerSession.isPlayerArrived());
    }

    private void init(boolean playerArrived) {
        this.playerArrived = playerArrived;
    }

    public boolean isPlayerArrived() {
        return playerArrived;
    }

    public void setPlayerArrived(boolean playerArrived) {
        this.playerArrived = playerArrived;
    }
}
