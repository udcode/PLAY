package com.play.data;

public class StaffMemberData extends BaseEntityData {
    private StaffMemberPermissionsEnum permissions;

    public StaffMemberData() {
    }

    public StaffMemberData(Integer id, StaffMemberPermissionsEnum permissions) {
        super(id);
        init(permissions);
    }

    public StaffMemberData(StaffMemberData staffMember) {
        super(staffMember);
        init(staffMember.getPermissions());
    }

    private void init(StaffMemberPermissionsEnum permissions) {
        this.permissions = permissions;
    }

    public StaffMemberPermissionsEnum getPermissions() {
        return permissions;
    }

    public void setPermissions(StaffMemberPermissionsEnum permissions) {
        this.permissions = permissions;
    }
}
