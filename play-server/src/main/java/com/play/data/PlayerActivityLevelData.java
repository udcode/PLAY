package com.play.data;

public class PlayerActivityLevelData extends BaseData {
    private Integer playerActivityLevel;

    public PlayerActivityLevelData() {
    }

    public PlayerActivityLevelData(Integer id, Integer playerActivityLevel) {
        super(id);
        init(playerActivityLevel);
    }

    public PlayerActivityLevelData(PlayerActivityLevelData playerActivityLevel) {
        super(playerActivityLevel);
        init(playerActivityLevel.getPlayerActivitylevel());
    }

    private void init(Integer playerActivityLevel) {
        this.playerActivityLevel = playerActivityLevel;
    }

    public Integer getPlayerActivitylevel() {
        return playerActivityLevel;
    }

    public void setPlayerActivityLevel(Integer playerActivityLevel) {
        this.playerActivityLevel = playerActivityLevel;
    }
}
