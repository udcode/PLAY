package com.play.data;

import java.util.Date;

public class SessionData extends BaseEntityData {
    private Integer minPlayers;
    private Integer maxPlayers;
    private Integer activityLevel;
    private Date startTime;
    private Date endTime;

    public SessionData() {
    }

    public SessionData(Integer id, Integer minPlayers, Integer maxPlayers, Integer activityLevel, Date startTime, Date endTime) {
        super(id);
        init(minPlayers, maxPlayers, activityLevel, startTime, endTime);
    }

    public SessionData(SessionData session) {
        super(session);
        init(session.getMinPlayers(), session.getMaxPlayers(), session.getActivityLevel(), session.getStartTime(), session.getEndTime());
    }

    private void init(Integer minPlayers, Integer maxPlayers, Integer activityLevel, Date startTime, Date endTime) {
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
        this.activityLevel = activityLevel;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(Integer minPlayers) {
        this.minPlayers = minPlayers;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public Integer getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(Integer activityLevel) {
        this.activityLevel = activityLevel;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
