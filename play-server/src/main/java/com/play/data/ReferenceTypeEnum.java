package com.play.data;

import com.play.utils.EnumUtil;

public enum ReferenceTypeEnum implements BaseEnum{
    user(1, "User"),
    facebook(2, "Facebook"),
    google(3, "Google"),
    website(4, "Website"),
    passedBy(5, "Passed by"),
    affiliate(6, "Affiliate");

    private int numberValue;
    private String displayValue;

    private ReferenceTypeEnum(int numberValue, String displayValue) {
        this.numberValue = numberValue;
        this.displayValue = displayValue;
        EnumUtil.get().add(this);
    }

    @Override
    public int getNumberValue() {
        return numberValue;
    }

    @Override
    public String getDisplayValue() {
        return displayValue;
    }
}
