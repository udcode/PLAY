package com.play.data;

public class PlaceData extends BaseEntityData {
    private String placeName;
    private String country;
    private String city;
    private String address;
    private Integer minPlayers;
    private Integer maxPlayers;
    private Integer latitude;
    private Integer longitude;

    public PlaceData() {
    }

    public PlaceData(Integer id, String placeName, String country, String city, String address, Integer minPlayers, Integer maxPlayers, Integer latitude, Integer longitude) {
        super(id);
        init(placeName, country, city, address, minPlayers, maxPlayers, latitude, longitude);
    }

    public PlaceData(PlaceData place) {
        super(place);
        init(place.getPlaceName(), place.getCountry(), place.getCity(), place.getAddress(), place.getMinPlayers(),
                place.getMaxPlayers(), place.getLatitude(), place.getLongitude());
    }

    private void init(String placeName, String country, String city, String address, Integer minPlayers, Integer maxPlayers, Integer latitude, Integer longitude) {
        this.placeName = placeName;
        this.country = country;
        this.city = city;
        this.address = address;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(Integer minPlayers) {
        this.minPlayers = minPlayers;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public Integer getLatitude() {
        return latitude;
    }

    public void setLatitude(Integer latitude) {
        this.latitude = latitude;
    }

    public Integer getLongitude() {
        return longitude;
    }

    public void setLongitude(Integer longitude) {
        this.longitude = longitude;
    }
}