package com.play.data;

import com.play.utils.EnumUtil;

public enum GenderEnum implements BaseEnum {
    male(1, "Male"),
    female(2, "Female");

    private int numberValue;
    private String displayValue;

    private GenderEnum(int numberValue, String displayValue) {
        this.numberValue = numberValue;
        this.displayValue = displayValue;
        EnumUtil.get().add(this);
    }

    @Override
    public int getNumberValue() {
        return numberValue;
    }

    @Override
    public String getDisplayValue() {
        return displayValue;
    }
}
