package com.play.data;

import com.play.utils.EnumUtil;

public enum UserEventTypeEnum implements BaseEnum{
    type1(1, "Type 1"),
    type2(2, "Type 2"),
    type3(3, "Type 3");

    private int numberValue;
    private String displayValue;

    private UserEventTypeEnum(int numberValue, String displayValue) {
        this.numberValue = numberValue;
        this.displayValue = displayValue;
        EnumUtil.get().add(this);
    }

    @Override
    public int getNumberValue() {
        return numberValue;
    }

    @Override
    public String getDisplayValue() {
        return displayValue;
    }
}
