package com.play.data;

public class PlayerData extends BaseEntityData {
    public PlayerData() {
    }

    public PlayerData(Integer id) {
        super(id);
    }

    public PlayerData(PlayerData player) {
        super(player);
    }
}
