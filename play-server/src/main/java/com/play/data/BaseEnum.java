package com.play.data;

public interface BaseEnum {
    public int getNumberValue();
    public String getDisplayValue();
}
