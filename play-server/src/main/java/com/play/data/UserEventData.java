package com.play.data;

import java.util.Date;

public class UserEventData extends BaseEntityData {
    private UserEventTypeEnum eventType;
    private Enum subEvent;
    private String details;
    private Date followUpTime;

    public UserEventData() {
    }

    public UserEventData(Integer id, UserEventTypeEnum eventType, Enum subEvent, String details, Date followUpTime) {
        super(id);
        init(eventType, subEvent, details, followUpTime);
    }

    public UserEventData(UserEventData userEvent) {
        super(userEvent);
        init(userEvent.getEventType(), userEvent.getSubEvent(), userEvent.getDetails(), userEvent.getFollowUpTime());
    }

    private void init(UserEventTypeEnum eventType, Enum subEvent, String details, Date followUpTime) {
        this.eventType = eventType;
        this.subEvent = subEvent;
        this.details = details;
        this.followUpTime = followUpTime;
    }

    public UserEventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(UserEventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public Enum getSubEvent() {
        return subEvent;
    }

    public void setSubEvent(Enum subEvent) {
        this.subEvent = subEvent;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getFollowUpTime() {
        return followUpTime;
    }

    public void setFollowUpTime(Date followUpTime) {
        this.followUpTime = followUpTime;
    }
}
