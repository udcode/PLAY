package com.play.data;

import java.util.Date;

public abstract class BaseEntityData extends BaseData {
    private Date creationTime;
    private String createdBy;
    private Date lastModificationTime;
    private String lastModifiedBy;
    private boolean active = true;

    protected BaseEntityData() {
    }

    protected BaseEntityData(Integer id) {
        super(id);
    }

    protected BaseEntityData(BaseData baseData) {
        super(baseData);
    }

    protected BaseEntityData(BaseEntityData entity) {
        super(entity);
        this.creationTime = entity.getCreationTime();
        this.createdBy = entity.getCreatedBy();
        this.lastModificationTime = entity.getLastModificationTime();
        this.lastModifiedBy = entity.getLastModifiedBy();
        this.active = entity.isActive();
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastModificationTime() {
        return lastModificationTime;
    }

    public void setLastModificationTime(Date lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
