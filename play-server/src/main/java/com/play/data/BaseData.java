package com.play.data;

public abstract class BaseData {
    private Integer id;

    protected BaseData() {
    }

    protected BaseData(Integer id) {
        init(id);
    }

    protected BaseData(BaseData baseData) {
        init(baseData.getId());
    }

    private void init(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ":" + (id == null ? "new" : id.toString());
    }
}
