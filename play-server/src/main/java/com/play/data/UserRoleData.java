package com.play.data;

public class UserRoleData extends BaseData {
    private UserRoleEnum role;

    public UserRoleData() {
    }

    public UserRoleData(Integer id, UserRoleEnum role) {
        super(id);
        init(role);
    }

    public UserRoleData(UserRoleData userRole) {
        super(userRole);
        init(userRole.getRole());
    }

    private void init(UserRoleEnum role) {
        this.role = role;
    }

    public UserRoleEnum getRole() {
        return role;
    }

    public void setRole(UserRoleEnum role) {
        this.role = role;
    }
}
