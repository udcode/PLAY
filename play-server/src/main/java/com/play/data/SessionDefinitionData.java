package com.play.data;

public class SessionDefinitionData extends BaseEntityData {
    private String title;
    private Integer minPlayers;
    private Integer maxPlayers;
    private Integer activityLevel;

    public SessionDefinitionData() {
    }

    public SessionDefinitionData(Integer id, String title, Integer minPlayers, Integer maxPlayers, Integer activityLevel) {
        super(id);
        init(title, minPlayers, maxPlayers, activityLevel);
    }

    public SessionDefinitionData(SessionDefinitionData sessionDefinition) {
        super(sessionDefinition);
        init(sessionDefinition.getTitle(), sessionDefinition.getMinPlayers(), sessionDefinition.getMaxPlayers(), sessionDefinition.getActivityLevel());
    }

    private void init(String title, Integer minPlayers, Integer maxPlayers, Integer activityLevel) {
        this.title = title;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
        this.activityLevel = activityLevel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(Integer minPlayers) {
        this.minPlayers = minPlayers;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public Integer getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(Integer activityLevel) {
        this.activityLevel = activityLevel;
    }
}
