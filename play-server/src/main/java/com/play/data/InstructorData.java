package com.play.data;

public class InstructorData extends BaseEntityData {
    public InstructorData() {
    }

    public InstructorData(Integer id) {
        super(id);
    }

    public InstructorData(InstructorData instructor) {
        super(instructor);
    }
}
