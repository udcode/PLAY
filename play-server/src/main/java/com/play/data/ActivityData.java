package com.play.data;

public class ActivityData extends BaseEntityData {
    private String title;
    private String description;
    private byte[] picture;

    public ActivityData() {
    }

    public ActivityData(Integer id, String title, String description, byte[] picture) {
        super(id);
        init(title, description, picture);
    }

    public ActivityData(ActivityData activity) {
        super(activity);
        init(activity.getTitle(), activity.getDescription(), activity.getPicture());
    }

    private void init (String title, String description, byte[] picture) {
        this.title = title;
        this.description = description;
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }
}
