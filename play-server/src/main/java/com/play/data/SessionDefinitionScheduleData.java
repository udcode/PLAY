package com.play.data;

import java.util.Date;

public class SessionDefinitionScheduleData extends BaseEntityData {
    private DayOfWeekEnum dayOfWeek;
    private Date startDate;
    private Date endDate;
    private Date startTime;
    private Date endTime;

    public SessionDefinitionScheduleData() {
    }

    public SessionDefinitionScheduleData(int id, DayOfWeekEnum dayOfWeek, Date startDate, Date endDate, Date startTime, Date endTime) {
        super(id);
        init(dayOfWeek, startDate, endDate, startTime, endTime);
    }

    public SessionDefinitionScheduleData(SessionDefinitionScheduleData sessionDefinitionSchedule) {
        super(sessionDefinitionSchedule);
        init(sessionDefinitionSchedule.getDayOfWeek(), sessionDefinitionSchedule.getStartDate(), sessionDefinitionSchedule.getEndDate(),
                sessionDefinitionSchedule.getStartTime(), sessionDefinitionSchedule.getEndTime());
    }

    private void init(DayOfWeekEnum dayOfWeek, Date startDate, Date endDate, Date startTime, Date endTime) {
        this.dayOfWeek = dayOfWeek;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public DayOfWeekEnum getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeekEnum dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
