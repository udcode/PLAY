package com.play.data;

import com.play.utils.EnumUtil;

public enum CurrencyEnum implements BaseEnum {
    nis(1, "NIS"),
    usd(2, "USD");

    private int numberValue;
    private String displayValue;

    private CurrencyEnum(int numberValue, String displayValue) {
        this.numberValue = numberValue;
        this.displayValue = displayValue;
        EnumUtil.get().add(this);
    }

    @Override
    public int getNumberValue() {
        return numberValue;
    }

    @Override
    public String getDisplayValue() {
        return displayValue;
    }
}
