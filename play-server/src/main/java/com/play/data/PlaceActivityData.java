package com.play.data;

public class PlaceActivityData extends BaseData {
    private Integer minPlayers;
    private Integer maxPlayers;

    public PlaceActivityData() {
    }

    public PlaceActivityData(Integer id, Integer minPlayers, Integer maxPlayers) {
        super(id);
        init(minPlayers, maxPlayers);
    }

    public PlaceActivityData(PlaceActivityData placeActivity) {
        super(placeActivity);
        init(placeActivity.getMinPlayers(), placeActivity.getMaxPlayers());
    }

    private void init(Integer minPlayers, Integer maxPlayers) {
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
    }

    public Integer getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(Integer minPlayers) {
        this.minPlayers = minPlayers;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }
}