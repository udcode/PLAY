package com.play.logic;

import com.play.data.SessionData;
import com.play.data.SessionDefinitionData;
import com.play.data.SessionDefinitionScheduleData;
import com.play.dbmanagers.SessionDbManager;
import com.play.dbmanagers.SessionDefinitionDbManager;
import com.play.dbmanagers.SessionDefinitionScheduleDbManager;
import com.play.view.SessionDefinitionScheduleView;
import com.play.view.SessionDefinitionView;
import com.play.view.SessionView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
@Transactional
public class SessionLogic {
    @Autowired
    private SessionDefinitionDbManager sessionDefinitionDbManager;
    @Autowired
    private SessionDbManager sessionDbManager;
    @Autowired
    private SessionDefinitionScheduleDbManager sessionDefinitionScheduleDbManager;

    private static Logger log = LogManager.getLogger(SessionLogic.class);

    public Collection<SessionDefinitionView> getAllSessionDefinitions() {
        return sessionDefinitionDbManager.getAllViews();
    }

    public Collection<SessionDefinitionView> findSessionDefinitionsByInstructor(int instructorId) {
        return sessionDefinitionDbManager.findViewsByInstructorId(instructorId);
    }

    public Collection<SessionDefinitionView> findSessionDefinitionsByPlace(int placeId) {
        return sessionDefinitionDbManager.findViewsByPlaceId(placeId);
    }

    public Collection<SessionDefinitionView> findSessionDefinitionsByMembership(int membershipId) {
        return sessionDefinitionDbManager.findViewsByMembershipId(membershipId);
    }

    public Collection<SessionDefinitionView> findSessionDefinitionsByActivity(int activityId) {
        return sessionDefinitionDbManager.findViewsByActivityId(activityId);
    }

    public SessionDefinitionView getSessionDefinition(int sessionDefinitionId) {
        return sessionDefinitionDbManager.getViewById(sessionDefinitionId);
    }

    public SessionDefinitionView createSessionDefinition(SessionDefinitionData sessionDefinition, int instructorId, int placeId, int membershipId, int activityId) {
        return sessionDefinitionDbManager.createSessionDefinition(sessionDefinition, instructorId, placeId, membershipId, activityId);
    }

    public SessionDefinitionView updateSessionDefinition(SessionDefinitionData sessionDefinition, int instructorId, int placeId, int membershipId, int activityId) {
        return sessionDefinitionDbManager.updateSessionDefinition(sessionDefinition, instructorId, placeId, membershipId, activityId);
    }

    public void deleteSessionDefinition(int sessionDefinitionId) {
        sessionDefinitionDbManager.deleteById(sessionDefinitionId);
    }

    public Collection<SessionDefinitionScheduleData> findSessionDefinitionSchedulesBySessionDefinition(int sessionDefinitionId) {
        return sessionDefinitionScheduleDbManager.findDatasBySessionDefinitionId(sessionDefinitionId);
    }

    public SessionDefinitionScheduleData getSessionDefinitionSchedule(int sessionDefinitionScheduleId) {
        return sessionDefinitionScheduleDbManager.getDataById(sessionDefinitionScheduleId);
    }

    public SessionDefinitionScheduleView createSessionDefinitionSchedule(SessionDefinitionScheduleData sessionDefinitionSchedule, int sessionDefinitionId) {
        return sessionDefinitionScheduleDbManager.createSessionDefinitionSchedule(sessionDefinitionSchedule, sessionDefinitionId);
    }

    public SessionDefinitionScheduleView updateSessionDefinitionSchedule(SessionDefinitionScheduleData sessionDefinitionSchedule, int sessionDefinitionId) {
        return sessionDefinitionScheduleDbManager.updateSessionDefinitionSchedule(sessionDefinitionSchedule, sessionDefinitionId);
    }

    public void deleteSessionDefinitionSchedule(int sessionDefinitionScheduleId) {
        sessionDefinitionScheduleDbManager.deleteById(sessionDefinitionScheduleId);
    }

    public Collection<SessionView> getAllSessions() {
        return sessionDbManager.getAllViews();
    }

    public Collection<SessionView> findSessionsBySessionDefinition(int sessionDefinitionId) {
        return sessionDbManager.findViewsBySessionDefinitionId(sessionDefinitionId);
    }

    public Collection<SessionView> findSessionsByInstructor(int instructorId) {
        return sessionDbManager.findViewsByInstructorId(instructorId);
    }

    public Collection<SessionView> findSessionsByPlace(int placeId) {
        return sessionDbManager.findViewsByPlaceId(placeId);
    }

    public Collection<SessionView> findSessionsByMembership(int membershipId) {
        return sessionDbManager.findViewsByMembershipId(membershipId);
    }

    public SessionView getSession(int sessionId) {
        return sessionDbManager.getViewById(sessionId);
    }

    public SessionView createSession(SessionData session, int sessionDefinitionId, int instructorId, int placeId, int membershipId) {
        return sessionDbManager.createSession(session, sessionDefinitionId, instructorId, placeId, membershipId);
    }

    public SessionView updateSession(SessionData session, int sessionDefinitionId, int instructorId, int placeId, int membershipId) {
        return sessionDbManager.updateSession(session, sessionDefinitionId, instructorId, placeId, membershipId);
    }

    public void deleteSession(int sessionId) {
        sessionDbManager.deleteById(sessionId);
    }
}