package com.play.logic;

import com.play.data.ActivityData;
import com.play.data.ActivityTypeData;
import com.play.dbmanagers.ActivityDbManager;
import com.play.dbmanagers.ActivityTypeDbManager;
import com.play.view.ActivityView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
@Transactional
public class ActivityLogic {
    @Autowired
    private ActivityDbManager activityDbManager;
    @Autowired
    private ActivityTypeDbManager activityTypeDbManager;

    private static Logger log = LogManager.getLogger(ActivityLogic.class);

    public Collection<ActivityView> getAllActivities() {
        return activityDbManager.getAllViews();
    }

    public Collection<ActivityData> findActivitiesByActivityType(int activityTypeId) {
        return activityDbManager.findDatasByActivityTypeId(activityTypeId);
    }

    public ActivityView getActivity(int activityId) {
        return activityDbManager.getViewById(activityId);
    }

    public ActivityView createActivity(ActivityData activity, int activityTypeId) {
        return activityDbManager.createActivity(activity, activityTypeId);
    }

    public ActivityView updateActivity(ActivityData activity, int activityTypeId) {
        return activityDbManager.updateActivity(activity, activityTypeId);
    }

    public void deleteActivity(int activityId) {
        activityDbManager.deleteById(activityId);
    }

    public Collection<ActivityTypeData> getAllActivityTypes() {
        return activityTypeDbManager.getAllDatas();
    }

    public ActivityTypeData getActivityType(int activityTypeId) {
        return activityTypeDbManager.getDataById(activityTypeId);
    }

    public ActivityTypeData createActivityType(ActivityTypeData activityType) {
        return activityTypeDbManager.createActivityType(activityType);
    }

    public ActivityTypeData updateActivityType(ActivityTypeData activityType) {
        return activityTypeDbManager.updateActivityType(activityType);
    }

    public void deleteActivityType(int activityTypeId) {
        activityTypeDbManager.deleteById(activityTypeId);
    }
}