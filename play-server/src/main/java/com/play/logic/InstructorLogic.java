package com.play.logic;

import com.play.dbmanagers.InstructorDbManager;
import com.play.view.InstructorView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
@Transactional
public class InstructorLogic {
    @Autowired
    private InstructorDbManager instructorDbManager;

    private static Logger log = LogManager.getLogger(InstructorLogic.class);

    public Collection<InstructorView> getAllInstructors() {
        return instructorDbManager.getAllViews();
    }

    public InstructorView getInstructor(int instructorId) {
        return instructorDbManager.getViewById(instructorId);
    }

    public InstructorView getInstructorByUser(int userId) {
        return instructorDbManager.getViewByUserId(userId);
    }
}