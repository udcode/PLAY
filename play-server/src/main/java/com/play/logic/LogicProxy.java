package com.play.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class LogicProxy {
    private static Logger log = LogManager.getLogger(LogicProxy.class);

    @Around("execution(* com.play.logic.*Logic.*(..))")
    public Object aroundLogicService(ProceedingJoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        Object[] methodArgs = joinPoint.getArgs();
        StringBuilder sb = new StringBuilder();
        sb.append(methodName);
        sb.append(" service was called");
        if (methodArgs.length > 0) {
            sb.append(" with arguments: ");
            sb.append(Arrays.toString(methodArgs));
        }
        log.info(sb.toString());
        try {
            return joinPoint.proceed();
        }
        catch (Throwable throwable) {
            log.error(throwable);
            if (throwable instanceof RuntimeException) {
                throw (RuntimeException)throwable;
            }
            throw new OperationFailedException(throwable);
        }
    }
}
