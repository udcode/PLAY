package com.play.logic;

import com.play.data.PlayerActivityLevelData;
import com.play.data.PlayerSessionData;
import com.play.dbmanagers.PlayerActivityLevelDbManager;
import com.play.dbmanagers.PlayerDbManager;
import com.play.dbmanagers.PlayerSessionDbManager;
import com.play.view.PlayerActivityLevelView;
import com.play.view.PlayerSessionView;
import com.play.view.PlayerView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
@Transactional
public class PlayerLogic {
    @Autowired
    private PlayerDbManager playerDbManager;
    @Autowired
    private PlayerActivityLevelDbManager playerActivityLevelDbManager;
    @Autowired
    private PlayerSessionDbManager playerSessionDbManager;

    private static Logger log = LogManager.getLogger(PlayerLogic.class);

    public Collection<PlayerView> getAllPlayers() {
        return playerDbManager.getAllViews();
    }

    public Collection<PlayerView> findPlayersByMembership(int membershipId) {
        return playerDbManager.findViewsByMembershipId(membershipId);
    }

    public PlayerView getPlayer(int playerId) {
        return playerDbManager.getViewById(playerId);
    }

    public PlayerView getPlayerByUser(int userId) {
        return playerDbManager.getViewByUserId(userId);
    }

    public Collection<PlayerActivityLevelView> findPlayerActivityLevelsByPlayer(int playerId) {
        return playerActivityLevelDbManager.findViewsByPlayerId(playerId);
    }

    public Collection<PlayerActivityLevelView> findPlayerActivityLevelsByActivity(int activityId) {
        return playerActivityLevelDbManager.findViewsByActivityId(activityId);
    }

    public PlayerActivityLevelView getPlayerActivityLevel(int playerActivityLevelId) {
        return playerActivityLevelDbManager.getViewById(playerActivityLevelId);
    }

    public PlayerActivityLevelView createPlayerActivityLevel(PlayerActivityLevelData playerActivityLevel, int playerId, int activityId) {
        return playerActivityLevelDbManager.createPlayerActivityLevel(playerActivityLevel, playerId, activityId);
    }

    public PlayerActivityLevelView updatePlayerActivityLevel(PlayerActivityLevelData playerActivityLevel, int playerId, int activityId) {
        return playerActivityLevelDbManager.updatePlayerActivityLevel(playerActivityLevel, playerId, activityId);
    }

    public void deletePlayerActivityLevel(int playerActivityLevelId) {
        playerActivityLevelDbManager.deleteById(playerActivityLevelId);
    }

    public Collection<PlayerSessionView> findPlayerSessionsByPlayer(int playerId) {
        return playerSessionDbManager.findViewsByPlayerId(playerId);
    }

    public Collection<PlayerSessionView> findPlayerSessionsBySession(int sessionId) {
        return playerSessionDbManager.findViewsBySessionId(sessionId);
    }

    public PlayerSessionView createPlayerSession(PlayerSessionData playerSession, int playerId, int sessionId) {
        return playerSessionDbManager.createPlayerSession(playerSession, playerId, sessionId);
    }

    public PlayerSessionView updatePlayerSession(PlayerSessionData playerSession, int playerId, int sessionId) {
        return playerSessionDbManager.updatePlayerSession(playerSession, playerId, sessionId);
    }

    public void deletePlayerSession(int playerSessionId) {
        playerSessionDbManager.deleteById(playerSessionId);
    }
}