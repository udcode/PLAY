package com.play.logic;

import com.play.data.MembershipData;
import com.play.dbmanagers.MembershipDbManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
@Transactional
public class MembershipLogic {
    @Autowired
    private MembershipDbManager membershipDbManager;

    private static Logger log = LogManager.getLogger(MembershipLogic.class);

    public Collection<MembershipData> getAllMemberships() {
        return membershipDbManager.getAllDatas();
    }

    public MembershipData getMembership(int membershipId) {
        return membershipDbManager.getDataById(membershipId);
    }

    public MembershipData createMembership(MembershipData membership) {
        return membershipDbManager.createMembership(membership);
    }

    public MembershipData updateMembership(MembershipData membership) {
        return membershipDbManager.updateMembership(membership);
    }

    public void deleteMembership(int membershipId) {
        membershipDbManager.deleteById(membershipId);
    }
}
