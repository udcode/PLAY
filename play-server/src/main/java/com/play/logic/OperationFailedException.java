package com.play.logic;

public class OperationFailedException extends RuntimeException {
    public OperationFailedException(Throwable cause) {
        super(cause);
    }
}
