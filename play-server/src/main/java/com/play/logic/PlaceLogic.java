package com.play.logic;

import com.play.data.PlaceActivityData;
import com.play.data.PlaceData;
import com.play.dbmanagers.PlaceActivityDbManager;
import com.play.dbmanagers.PlaceDbManager;
import com.play.dbmanagers.PlaceOwnerDbManager;
import com.play.view.PlaceActivityView;
import com.play.view.PlaceOwnerView;
import com.play.view.PlaceView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
@Transactional
public class PlaceLogic {
    @Autowired
    private PlaceDbManager placeDbManager;
    @Autowired
    private PlaceOwnerDbManager placeOwnerDbManager;
    @Autowired
    private PlaceActivityDbManager placeActivityDbManager;

    private static Logger log = LogManager.getLogger(PlaceLogic.class);

    public Collection<PlaceView> getAllPlaces() {
        return placeDbManager.getAllViews();
    }

    public Collection<PlaceView> findPlacesByPlaceOwner(int placeOwnerId) {
        return placeDbManager.findViewsByPlaceOwnerId(placeOwnerId);
    }

    public PlaceView getPlace(int placeId) {
        return placeDbManager.getViewById(placeId);
    }

    public PlaceView createPlace(PlaceData place, int placeOwnerId) {
        return placeDbManager.createPlace(place, placeOwnerId);
    }

    public PlaceView updatePlace(PlaceData place, int placeOwnerId) {
        return placeDbManager.updatePlace(place, placeOwnerId);
    }

    public void deletePlace(int placeId) {
        placeDbManager.deleteById(placeId);
    }

    public Collection<PlaceOwnerView> getAllPlaceOwners() {
        return placeOwnerDbManager.getAllViews();
    }

    public PlaceOwnerView getPlaceOwner(int placeOwnerId) {
        return placeOwnerDbManager.getViewById(placeOwnerId);
    }

    public PlaceOwnerView getPlaceOwnerByUser(int userId) {
        return placeOwnerDbManager.getViewByUserId(userId);
    }

    public Collection<PlaceActivityView> findPlaceActivitiesByPlace(int placeId) {
        return placeActivityDbManager.findViewsByPlaceId(placeId);
    }

    public Collection<PlaceActivityView> findPlaceActivitiesByActivity(int activityId) {
        return placeActivityDbManager.findViewsByActivityId(activityId);
    }

    public PlaceActivityView createPlaceActivity(PlaceActivityData placeActivity, int placeId, int activityId) {
        return placeActivityDbManager.createPlaceActivity(placeActivity, placeId, activityId);
    }

    public PlaceActivityView updatePlaceActivity(PlaceActivityData placeActivity, int placeId, int activityId) {
        return placeActivityDbManager.updatePlaceActivity(placeActivity, placeId, activityId);
    }

    public void deletePlaceActivity(int placeActivityId) {
        placeActivityDbManager.deleteById(placeActivityId);
    }
}
