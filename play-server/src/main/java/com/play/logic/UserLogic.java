package com.play.logic;

import com.play.data.*;
import com.play.dbmanagers.*;
import com.play.view.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
@Transactional
public class UserLogic {
    @Autowired
    private UserDbManager userDbManager;
    @Autowired
    private UserEventDbManager userEventDbManager;
    @Autowired
    private PlayerDbManager playerDbManager;
    @Autowired
    private InstructorDbManager instructorDbManager;
    @Autowired
    private PlaceOwnerDbManager placeOwnerDbManager;
    @Autowired
    private StaffMemberDbManager staffMemberDbManager;

    private static Logger log = LogManager.getLogger(UserLogic.class);

    public Collection<UserView> getAllUsers() {
        return userDbManager.getAllViews();
    }

    public Collection<UserView> findUsersByReferenceUser(int referenceUserId) {
        return userDbManager.findViewsByReferenceUserId(referenceUserId);
    }

    public UserView getUser(int userId) {
        return userDbManager.getViewById(userId);
    }

    public UserView createUser(UserData user, Collection<UserRoleEnum> roles, Integer referenceUserId, Integer playerMembershipId, StaffMemberPermissionsEnum staffMemberPermissions) {
        if (roles == null || roles.isEmpty()) {
            throw new IllegalArgumentException("Cannot create user without roles");
        }

        UserView userView = userDbManager.createUser(user, referenceUserId, roles);

        if (userView.isPlayer()) {
            if (playerMembershipId == null) {
                throw new IllegalArgumentException("Cannot create player with membershipId = null");
            }
            PlayerData player = new PlayerData();
            playerDbManager.createPlayer(player, userView.getId(), playerMembershipId);
        }

        if (userView.isInstructor()) {
            InstructorData instructor = new InstructorData();
            instructorDbManager.createInstructor(instructor, userView.getId());
        }

        if (userView.isPlaceOwner()) {
            PlaceOwnerData placeOwner = new PlaceOwnerData();
            placeOwnerDbManager.createPlaceOwner(placeOwner, userView.getId());
        }

        if (userView.isStaffMember()) {
            if (staffMemberPermissions == null) {
                throw new IllegalArgumentException("Cannot create staff member with permissions = null");
            }
            StaffMemberData staffMember = new StaffMemberData();
            staffMember.setPermissions(staffMemberPermissions);
            staffMemberDbManager.createStaffMember(staffMember, userView.getId());
        }

        return userView;
    }

    public UserView updateUser(UserData user, Collection<UserRoleEnum> roles, Integer referenceUserId, Integer playerMembershipId, StaffMemberPermissionsEnum staffMemberPermissions) {
        UserView userView = userDbManager.updateUser(user, referenceUserId, roles);
        PlayerView currentPlayer = playerDbManager.getViewByUserId(userView.getId());
        InstructorView currentInstructor = instructorDbManager.getViewByUserId(userView.getId());
        PlaceOwnerView currentPlaceOwner = placeOwnerDbManager.getViewByUserId(userView.getId());
        StaffMemberView currentStaffMember = staffMemberDbManager.getViewByUserId(userView.getId());

        if (currentPlayer == null) {
            if (userView.isPlayer()) {
                if (playerMembershipId == null) {
                    throw new IllegalArgumentException("Cannot create player with membershipId = null");
                }
                PlayerData newPlayer = new PlayerData();
                playerDbManager.createPlayer(newPlayer, userView.getId(), playerMembershipId);
            }
        }
        else {
            currentPlayer.setActive(userView.isPlayer());
            playerDbManager.updatePlayer(currentPlayer, userView.getId(), (playerMembershipId != null && userView.isPlayer()) ? playerMembershipId : currentPlayer.getMembership().getId());
        }

        if (currentInstructor == null) {
            if (userView.isInstructor()) {
                InstructorData instructor = new InstructorData();
                instructorDbManager.createInstructor(instructor, userView.getId());
            }
        }
        else {
            currentInstructor.setActive(userView.isInstructor());
            instructorDbManager.updateInstructor(currentInstructor, userView.getId());
        }

        if (currentPlaceOwner == null) {
            if (userView.isPlaceOwner()) {
                PlaceOwnerData placeOwner = new PlaceOwnerData();
                placeOwnerDbManager.createPlaceOwner(placeOwner, userView.getId());
            }
        }
        else {
            currentPlaceOwner.setActive(userView.isPlaceOwner());
            placeOwnerDbManager.updatePlaceOwner(currentPlaceOwner, userView.getId());
        }

        if (currentStaffMember == null) {
            if (userView.isStaffMember()) {
                if (staffMemberPermissions == null) {
                    throw new IllegalArgumentException("Cannot create staff member with permissions = null");
                }
                StaffMemberData staffMember = new StaffMemberData();
                staffMember.setPermissions(staffMemberPermissions);
                staffMemberDbManager.createStaffMember(staffMember, userView.getId());
            }
        }
        else {
            currentStaffMember.setActive(userView.isStaffMember());
            if (staffMemberPermissions != null && userView.isStaffMember()) {
                currentStaffMember.setPermissions(staffMemberPermissions);
            }
            staffMemberDbManager.updateStaffMember(currentStaffMember, userView.getId());
        }

        return userView;
    }

    public void deleteUser(int userId) {
        userDbManager.deleteById(userId);
    }

    public Collection<UserEventView> getAllUserEvents() {
        return userEventDbManager.getAllViews();
    }

    public Collection<UserEventView> findUserEventsByUser(int userId) {
        return userEventDbManager.findViewsByUserId(userId);
    }

    public Collection<UserEventView> findUserEventsByStaffMember(int staffMemberId) {
        return userEventDbManager.findViewsByStaffMemberId(staffMemberId);
    }

    public UserEventView getUserEvent(int userEventId) {
        return userEventDbManager.getViewById(userEventId);
    }

    public UserEventView createUserEvent(UserEventData userEvent, int userId, int staffMemberId) {
        return userEventDbManager.createUserEvent(userEvent, userId, staffMemberId);
    }

    public UserEventView updateUserEvent(UserEventData userEvent, int userId, int staffMemberId) {
        return userEventDbManager.updateUserEvent(userEvent, userId, staffMemberId);
    }

    public void deleteUserEvent(int userEventId) {
        userEventDbManager.deleteById(userEventId);
    }
}