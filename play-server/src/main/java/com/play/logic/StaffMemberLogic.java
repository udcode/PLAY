package com.play.logic;

import com.play.dbmanagers.StaffMemberDbManager;
import com.play.view.StaffMemberView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
@Transactional
public class StaffMemberLogic {
    @Autowired
    private StaffMemberDbManager staffMemberDbManager;

    private static Logger log = LogManager.getLogger(StaffMemberLogic.class);

    public Collection<StaffMemberView> getAllStaffMembers() {
        return staffMemberDbManager.getAllViews();
    }

    public StaffMemberView getStaffMember(int staffMemberId) {
        return staffMemberDbManager.getViewById(staffMemberId);
    }

    public StaffMemberView getStaffMemberByUser(int userId) {
        return staffMemberDbManager.getViewByUserId(userId);
    }
}
