USE play;

#
# Memberships
#

INSERT INTO memberships (creation_time, created_by, last_modification_time, last_modified_by, active, membership_name, price, currency, membership_level)
    VALUES ('2017-10-01 22:12:47', 'anonymousUser', '2017-10-01 22:12:47', 'anonymousUser', true, 'Basic', '79', '2', '10');

INSERT INTO memberships (creation_time, created_by, last_modification_time, last_modified_by, active, membership_name, price, currency, membership_level)
    VALUES ('2017-10-01 22:13:08', 'anonymousUser', '2017-10-01 22:13:08', 'anonymousUser', true, 'Pro', '99', '2', '50');

INSERT INTO memberships (creation_time, created_by, last_modification_time, last_modified_by, active, membership_name, price, currency, membership_level)
    VALUES ('2017-10-01 22:12:47', 'anonymousUser', '2017-10-01 22:12:47', 'anonymousUser', true, 'Premium', '129', '2', '100');

#
# Players
#

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
        VALUES('2017-10-15 16:32:07', 'anonymousUser', '2017-10-15 16:32:07', 'anonymousUser', true, 'Marc', 'Newman', '+1 831-281-7466', 'marc.nemman.2@gmail.com', 'testpic', null, '1', '474 Mission St.', 'San Francisco', 'California', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(1, 1);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-15 16:32:07', 'anonymousUser', '2017-10-15 16:32:07', 'anonymousUser', true, '1', '2');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-17 08:54:08', 'anonymousUser', '2017-10-17 08:54:08', 'anonymousUser', true, 'Michelle', 'Osmond', '+1 415-776-4543', 'michelle.osmond@yahoo.com', 'testpic', null, '2', '16151 El Camino Real', 'San Mateo', 'California', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(2, 1);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-17 08:54:08', 'anonymousUser', '2017-10-17 08:54:08', 'anonymousUser', true, '2', '1');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-17 11:32:27', 'anonymousUser', '2017-10-17 11:32:27', 'anonymousUser', true, 'Stewart', 'Cobb', '+1 212-854-5352', 'cob2755@gmail.com', 'testpic', null, '1', '1799 3rd Av.', 'New York', 'New York', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(3, 1);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-17 11:32:27', 'anonymousUser', '2001-01-01', '2017-10-17 11:32:27', true, '3', '2');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-17 16:55:42', 'anonymousUser', '2017-10-17 16:55:42', 'anonymousUser', true, 'Tal', 'Sheffer', '+972 54-800-0535', 'talshffr@gmail.com', 'testpic', null, '1', '6a Hamagid St.', 'Tel Aviv', null, 'Israel', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(4, 1);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-17 16:55:42', 'anonymousUser', '2017-10-17 16:55:42', 'anonymousUser', true, '4', '3');

#
# Instructors
#

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-15 21:44:47', 'anonymousUser', '2017-10-15 21:44:47', 'anonymousUser', true, 'Carl', 'Lewis', '+1 713-123-4567', 'carl727@gmail.com', 'testpic', null, '1', '7566 Oak St.', 'Houston', 'Texas', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(5, 1);

INSERT INTO users_roles (user_id, role)
    VALUES(5, 2);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-15 21:44:47', 'anonymousUser', '2017-10-15 21:44:47', 'anonymousUser', true, '5', '2');

INSERT INTO instructors (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-15 21:44:47', 'anonymousUser', '2017-10-15 21:44:47', 'anonymousUser', true, '5');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-18 08:10:47', 'anonymousUser', '2017-10-18 08:10:47', 'anonymousUser', true, 'Tony', 'Cress', '+1 713-445-4567', 'tonytrx@gmail.com', 'testpic', null, '1', '5822 Mountain Rd.', 'Houston', 'Texas', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(6, 1);

INSERT INTO users_roles (user_id, role)
    VALUES(6, 2);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-18 08:10:47', 'anonymousUser', '2017-10-18 08:10:47', 'anonymousUser', true, '6', '2');

INSERT INTO instructors (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-18 08:10:47', 'anonymousUser', '2017-10-18 08:10:47', 'anonymousUser', true, '6');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-20 12:41:18', 'anonymousUser', '2017-10-20 12:41:18', 'anonymousUser', true, 'Chris', 'Marshal', '+1 212-616-7275', 'chris.marsh4@hotmail.com', 'testpic', null, '1', '145 68 St.', 'New York', 'New York', 'USA','$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(7, 2);

INSERT INTO instructors (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-20 12:41:18', 'anonymousUser', '2017-10-20 12:41:18', 'anonymousUser', true, '7');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-24 17:27:12', 'anonymousUser', '2017-10-24 17:27:12', 'anonymousUser', true, 'Yuval', 'Klein', '+972 54-453-0593', 'yuval@yk-group.org', 'testpic', null, '1', '11 Hapardes St.', 'Tel Aviv', null, 'Israel', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', 1, null, 4);

INSERT INTO users_roles (user_id, role)
    VALUES(8, 2);

INSERT INTO instructors (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-24 17:27:12', 'anonymousUser', '2017-10-24 17:27:12', 'anonymousUser', true, '8');

#
# Place Owners
#

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-19 20:41:50', 'anonymousUser', '2017-10-19 20:41:50', 'anonymousUser', true, 'Suzan', 'Cohen', '+1 415-845-3217', 'suzyccc@gmail.com', 'testpic', null, '2', '1175 Bay St.', 'San Francisco', 'California', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(9, 1);

INSERT INTO users_roles (user_id, role)
    VALUES(9, 2);

INSERT INTO users_roles (user_id, role)
    VALUES(9, 3);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-19 20:41:50', 'anonymousUser', '2017-10-19 20:41:50', 'anonymousUser', true, '9', '3');

INSERT INTO instructors (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-19 20:41:50', 'anonymousUser', '2017-10-19 20:41:50', 'anonymousUser', true, '9');

INSERT INTO place_owners (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-19 20:41:50', 'anonymousUser', '2017-10-19 20:41:50', 'anonymousUser', true, '9');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-21 19:16:22', 'anonymousUser', '2017-10-21 19:16:22', 'anonymousUser', true, 'Luc', 'Underwood', '+1 913-729-5488', 'lunderwood@gmail.com', 'testpic', null, '1', '8812 Lakeside Rd.', 'Houston', 'Textas', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', 1, null, 2);

INSERT INTO users_roles (user_id, role)
    VALUES(10, 2);

INSERT INTO users_roles (user_id, role)
    VALUES(10, 3);

INSERT INTO instructors (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-21 19:16:22', 'anonymousUser', '2017-10-21 19:16:22', 'anonymousUser', true, '10');

INSERT INTO place_owners (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-21 19:16:22', 'anonymousUser', '2017-10-21 19:16:22', 'anonymousUser', true, '10');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-21 21:55:31', 'anonymousUser', '2017-10-21 21:55:31', 'anonymousUser', true, 'Tanya', 'Allen', '+1 212-618-4200', 'tanyatan@tanyafitness.com', 'testpic', null, '2', '154 Stanton St.', 'New York', 'New york', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(11, 1);

INSERT INTO users_roles (user_id, role)
    VALUES(11, 3);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-21 21:55:31', 'anonymousUser', '2017-10-21 21:55:31', 'anonymousUser', true, '11', '2');

INSERT INTO place_owners (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-21 21:55:31', 'anonymousUser', '2017-10-21 21:55:31', 'anonymousUser', true, '11');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-22 10:28:32', 'anonymousUser', '2017-10-22 10:28:32', 'anonymousUser', true, 'Erez', 'Gilad', '+972 50-330-8341', 'egilad@gmail.com', 'testpic', null, '1', '53 Hadar St.', 'Herzelya', null, 'Israel','$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(12, 3);

INSERT INTO place_owners (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-22 10:28:32', 'anonymousUser', '2017-10-22 10:28:32', 'anonymousUser', true, '12');

#
# Staff Members
#

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-16 17:04:17', 'anonymousUser', '2017-10-16 17:04:17', 'anonymousUser', true, 'Shannon', 'Wright', '+1 415-860-2931', 'shannon.wright@play.com', 'testpic', null, '2', '815 Pine St.', 'San Francisco', 'California', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(13, 1);

INSERT INTO users_roles (user_id, role)
    VALUES(13, 3);

INSERT INTO users_roles (user_id, role)
    VALUES(13, 4);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-16 17:04:17', 'anonymousUser', '2017-10-16 17:04:17', 'anonymousUser', true, '13', '3');

INSERT INTO place_owners (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-16 17:04:17', 'anonymousUser', '2017-10-16 17:04:17', 'anonymousUser', true, '13');

INSERT INTO staff_members (creation_time, created_by, last_modification_time, last_modified_by, active, permissions, user_id)
    VALUES ('2017-10-16 17:04:17', 'anonymousUser', '2017-10-16 17:04:17', 'anonymousUser', true, '1', '13');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-17 11:36:22', 'anonymousUser', '2017-10-17 11:36:22', 'anonymousUser', true, 'Tali', 'Shemesh', '+972 54-229-7611', 'ShemeshTali@gmail.com', 'testpic', null, '2', '33 Hanegev St.', 'Raanana', null, 'Israel', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', 1, null, 8);

INSERT INTO users_roles (user_id, role)
    VALUES(14, 1);

INSERT INTO users_roles (user_id, role)
    VALUES(14, 2);

INSERT INTO users_roles (user_id, role)
    VALUES(14, 4);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-17 11:36:22', 'anonymousUser', '2017-10-17 11:36:22', 'anonymousUser', true, '14', '2');

INSERT INTO instructors (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-17 11:36:22', 'anonymousUser', '2017-10-17 11:36:22', 'anonymousUser', true, '14');

INSERT INTO staff_members (creation_time, created_by, last_modification_time, last_modified_by, active, permissions, user_id)
    VALUES ('2017-10-17 11:36:22', 'anonymousUser', '2017-10-17 11:36:22', 'anonymousUser', true, '2', '14');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-17 15:08:08', 'anonymousUser', '2017-10-17 15:08:08', 'anonymousUser', true, 'Amanda', 'Fischer', '+1 925-782-2783', 'amandafischer@play.com', 'testpic', null, '2', '10 Shaw Rd.', 'Walnut Creek', 'California', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', 1, null, 8);

INSERT INTO users_roles (user_id, role)
    VALUES(15, 1);

INSERT INTO users_roles (user_id, role)
    VALUES(15, 2);

INSERT INTO users_roles (user_id, role)
    VALUES(15, 3);

INSERT INTO users_roles (user_id, role)
    VALUES(15, 4);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-17 15:08:08', 'anonymousUser', '2017-10-17 15:08:08', 'anonymousUser', true, '15', '2');

INSERT INTO instructors (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-17 15:08:08', 'anonymousUser', '2017-10-17 15:08:08', 'anonymousUser', true, '15');

INSERT INTO place_owners (creation_time, created_by, last_modification_time, last_modified_by, active, user_id)
    VALUES ('2017-10-17 15:08:08', 'anonymousUser', '2017-10-17 15:08:08', 'anonymousUser', true, '15');

INSERT INTO staff_members (creation_time, created_by, last_modification_time, last_modified_by, active, permissions, user_id)
    VALUES ('2017-10-17 15:08:08', 'anonymousUser', '2017-10-17 15:08:08', 'anonymousUser', true, '3', '15');

INSERT INTO users (creation_time, created_by, last_modification_time, last_modified_by, active, first_name, last_name, phone_number, email, picture, birth_date, gender, address, city, state, country, password, reference_type, reference_details, reference_user_id)
    VALUES('2017-10-20 16:40:41', 'anonymousUser', '2017-10-20 16:40:41', 'anonymousUser', true, 'Gina', 'Hirsh', '+1 628-432-8417', 'ghirsh@gmail.com', 'testpic', null, '2', '6221 Octavia St.', 'San Francisco', 'California', 'USA', '$2a$10$dsbsNROPhsBhNTUx./AbDOsDdAlRRGQ0bWrUS9mdcJ78hpTY2HecW', null, null, null);

INSERT INTO users_roles (user_id, role)
    VALUES(16, 1);

INSERT INTO users_roles (user_id, role)
    VALUES(16, 4);

INSERT INTO players (creation_time, created_by, last_modification_time, last_modified_by, active, user_id, membership_id)
    VALUES ('2017-10-20 16:40:41', 'anonymousUser', '2017-10-20 16:40:41', 'anonymousUser', true, '16', '3');

INSERT INTO staff_members (creation_time, created_by, last_modification_time, last_modified_by, active, permissions, user_id)
    VALUES ('2017-10-20 16:40:41', 'anonymousUser', '2017-10-20 16:40:41', 'anonymousUser', true, '1', '16');

# insert place
# need to add contact info like phone, email and url and picture
INSERT INTO places (creation_time, created_by, last_modification_time, last_modified_by, active, place_name, country, city, address, max_players, latitude, longitude, place_owner_id)
    VALUES('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Urban PLAY', 'Israel', 'Tel Aviv', '4 ester amalka', '15', '111', '111', '1');

INSERT INTO places (creation_time, created_by, last_modification_time, last_modified_by, active, place_name, country, city, address, max_players, latitude, longitude, place_owner_id)
    VALUES('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Dance Club', 'Israel', 'Tel Aviv', '2 ashuk', '20', '111', '111', '2');

INSERT INTO places (creation_time, created_by, last_modification_time, last_modified_by, active, place_name, country, city, address, max_players, latitude, longitude, place_owner_id)
    VALUES('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Fight Place', 'Israel', 'Tel Aviv', '10 Rokach', '10', '111', '111', '3');

INSERT INTO places (creation_time, created_by, last_modification_time, last_modified_by, active, place_name, country, city, address, max_players, latitude, longitude, place_owner_id)
    VALUES('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Gym', 'Israel', 'Tel Aviv', '15 ben ami', '15', '111', '111', '4');

INSERT INTO places (creation_time, created_by, last_modification_time, last_modified_by, active, place_name, country, city, address, max_players, latitude, longitude, place_owner_id)
    VALUES('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Urban Gym', 'Israel', 'Tel Aviv', '1 yermiyau', '15', '111', '111', '5');

INSERT INTO places (creation_time, created_by, last_modification_time, last_modified_by, active, place_name, country, city, address, max_players, latitude, longitude, place_owner_id)
    VALUES('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'PLAYce', 'Israel', 'Tel Aviv', '4 ester ', '15', '111', '111', '6');

# insert activity types
INSERT INTO activity_types (creation_time, created_by, last_modification_time, last_modified_by, active, title, description, picture)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'dance', 'all kind of dance', 'testpic');

INSERT INTO activity_types (creation_time, created_by, last_modification_time, last_modified_by, active, title, description, picture)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Martial arts', 'all kind of martial arts', 'testpic');

INSERT INTO activity_types (creation_time, created_by, last_modification_time, last_modified_by, active, title, description, picture)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Yoga', 'all kind of yoga', 'testpic');

#insert activity
INSERT INTO activities (creation_time, created_by, last_modification_time, last_modified_by, active, title, description, picture, activity_type_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Yoga ashtanga', 'great for begginers', 'testpic', '3');

INSERT INTO activities (creation_time, created_by, last_modification_time, last_modified_by, active, title, description, picture, activity_type_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Zomba', 'come to dance with us', 'testpic', '1');

INSERT INTO activities (creation_time, created_by, last_modification_time, last_modified_by, active, title, description, picture, activity_type_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Krav maga', 'for beginners', 'testpic', '2');

INSERT INTO activities (creation_time, created_by, last_modification_time, last_modified_by, active, title, description, picture, activity_type_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Hip Hop', 'jump with us', 'testpic', '1');

#insert session definition
INSERT INTO session_definitions (creation_time, created_by, last_modification_time, last_modified_by, active, title, min_players, instructor_id, place_id, membership_id, activity_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Yoga ashtanga', '0', '1', '1', '2', '1');

INSERT INTO session_definitions (creation_time, created_by, last_modification_time, last_modified_by, active, title, min_players, instructor_id, place_id, membership_id, activity_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Zomba', '2', '2', '2', '2', '2');

INSERT INTO session_definitions (creation_time, created_by, last_modification_time, last_modified_by, active, title, min_players, instructor_id, place_id, membership_id, activity_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Krav maga', '1', '3', '3', '1', '3');

INSERT INTO session_definitions (creation_time, created_by, last_modification_time, last_modified_by, active, title, min_players, instructor_id, place_id, membership_id, activity_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Hip Hop', '3', '4', '4', '1', '4');

INSERT INTO session_definitions (creation_time, created_by, last_modification_time, last_modified_by, active, title, min_players, instructor_id, place_id, membership_id, activity_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Yoga ashtanga', '0', '5', '5', '1', '1');

INSERT INTO session_definitions (creation_time, created_by, last_modification_time, last_modified_by, active, title, min_players, instructor_id, place_id, membership_id, activity_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Yoga ashtanga', '0', '6', '6', '1', '1');

INSERT INTO session_definitions (creation_time, created_by, last_modification_time, last_modified_by, active, title, min_players, instructor_id, place_id, membership_id, activity_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Zomba', '0', '7', '1', '2', '2');

INSERT INTO session_definitions (creation_time, created_by, last_modification_time, last_modified_by, active, title, min_players, instructor_id, place_id, membership_id, activity_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Krav maga', '1', '8', '1', '1', '3');

INSERT INTO session_definitions (creation_time, created_by, last_modification_time, last_modified_by, active, title, min_players, instructor_id, place_id, membership_id, activity_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, 'Hip Hop', '0', '4', '3', '2', '4');

# insert session definitions schedule
INSERT INTO session_definition_schedules (creation_time, created_by, last_modification_time, last_modified_by, active, day_of_week, start_date, start_time, end_time, session_definition_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, '1', '2017-11-04', '15:00', '16:00', '1');

INSERT INTO session_definition_schedules (creation_time, created_by, last_modification_time, last_modified_by, active, day_of_week, start_date, start_time, end_time, session_definition_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, '2', '2017-11-04', '18:00', '19:00', '2');

INSERT INTO session_definition_schedules (creation_time, created_by, last_modification_time, last_modified_by, active, day_of_week, start_date, start_time, end_time, session_definition_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, '6', '2017-11-04', '10:00', '11:00', '3');

INSERT INTO session_definition_schedules (creation_time, created_by, last_modification_time, last_modified_by, active, day_of_week, start_date, start_time, end_time, session_definition_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, '5', '2017-11-04', '20:00', '21:00', '1');

#insert sessions
INSERT INTO sessions (creation_time, created_by, last_modification_time, last_modified_by, active, min_players, max_players, activity_level, start_time, end_time, session_definition_id, instructor_id, place_id, membership_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, '0', '20', '0', '2017-11-04 15:00', '2017-11-04 16:00', '1', '1', '1', '1');

INSERT INTO sessions (creation_time, created_by, last_modification_time, last_modified_by, active, min_players, max_players, activity_level, start_time, end_time, session_definition_id, instructor_id, place_id, membership_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, '1', '21', '1', '2017-11-04 18:00', '2017-11-04 19:00', '1', '2', '2', '2');

INSERT INTO sessions (creation_time, created_by, last_modification_time, last_modified_by, active, min_players, max_players, activity_level, start_time, end_time, session_definition_id, instructor_id, place_id, membership_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, '2', '10', '1', '2017-11-04 10:00', '2017-11-04 11:00', '2', '1', '3', '1');

INSERT INTO sessions (creation_time, created_by, last_modification_time, last_modified_by, active, min_players, max_players, activity_level, start_time, end_time, session_definition_id, instructor_id, place_id, membership_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, '5', '8', '3', '2017-11-04 20:00', '2017-11-04 21:00', '3', '3', '1', '2');

# insert user event
INSERT user_events (creation_time, created_by, last_modification_time, last_modified_by, active, event_type, sub_event, details, user_id, staff_member_id)
    VALUES ('2001-01-01', 'anonymousUser', '2001-01-01', 'anonymousUser', true, '1', null, 'need to call him tomorrow', '2', '1');

# insert instructor activity
INSERT INTO instructors_activities (instructor_id, activity_id)
    VALUES ('1', '1');

INSERT INTO instructors_activities (instructor_id, activity_id)
    VALUES ('2', '2');

INSERT INTO instructors_activities (instructor_id, activity_id)
    VALUES ('1', '4');

INSERT INTO instructors_activities (instructor_id, activity_id)
    VALUES ('4', '3');

INSERT INTO instructors_activities (instructor_id, activity_id)
    VALUES ('3', '3');

INSERT INTO instructors_activities (instructor_id, activity_id)
    VALUES ('5', '1');

# insert places activities
INSERT INTO places_activities (min_players, max_players, place_id, activity_id)
    VALUES ('4', '15', '1', '2');

INSERT INTO places_activities (min_players, max_players, place_id, activity_id)
    VALUES ('0', '4', '1', '3');

INSERT INTO places_activities (min_players, max_players, place_id, activity_id)
    VALUES ('2', '13', '2', '3');

INSERT INTO places_activities (min_players, max_players, place_id, activity_id)
    VALUES ('0', '25', '3', '4');

INSERT INTO places_activities (min_players, max_players, place_id, activity_id)
    VALUES ('0', '16', '3', '2');

INSERT INTO places_activities (min_players, max_players, place_id, activity_id)
    VALUES ('1', '2', '4', '1');

#insert player activity levels
INSERT INTO player_activity_levels (player_activity_level, player_id, activity_id)
    VALUES ('0', '1', '1');

INSERT INTO player_activity_levels (player_activity_level, player_id, activity_id)
    VALUES ('3', '1', '2');

INSERT INTO player_activity_levels (player_activity_level, player_id, activity_id)
    VALUES ('4', '1', '3');

INSERT INTO player_activity_levels (player_activity_level, player_id, activity_id)
    VALUES ('0', '2', '1');

INSERT INTO player_activity_levels (player_activity_level, player_id, activity_id)
    VALUES ('5', '2', '4');

# insert player sessions
INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('1', '1', '1');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('1', '1', '2');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('0', '2', '2');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('1', '4', '1');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('0', '5', '3');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('1', '6', '4');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('1', '2', '1');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('1', '3', '1');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('1', '5', '1');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('0', '6', '1');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('0', '7', '1');

INSERT INTO players_sessions (player_arrived, player_id, session_id)
    VALUES ('1', '8', '1');

