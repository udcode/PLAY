CREATE SCHEMA play;
USE play;

CREATE TABLE activity_types(
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # ActivityTypeData fields
    title VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    picture BLOB NOT NULL,

    # Indices and references
    PRIMARY KEY (id),
    UNIQUE INDEX activity_types_id_unique_index (id ASC),
    UNIQUE INDEX activity_types_title_unique_index (title ASC));

CREATE TABLE activities(
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # ActivityData fields
    title VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    picture BLOB NOT NULL,

    # Indices and references
    activity_type_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX activities_id_unique_index (id ASC),
    UNIQUE INDEX activities_title_unique_index (title ASC),
    INDEX activities_activity_type_id_index (activity_type_id ASC),
    CONSTRAINT fk_activities_activity_type
        FOREIGN KEY (activity_type_id)
        REFERENCES activity_types (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE users (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # UserData fields
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    phone_number VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    picture BLOB NOT NULL,
    birth_date DATE NULL,
    gender INT NOT NULL,
    address VARCHAR(50) NULL,
    city VARCHAR(50) NOT NULL,
    state VARCHAR(50) NULL,
    country VARCHAR(50) NOT NULL,
    password VARCHAR(200) NOT NULL,
    reference_type INT NULL,
    reference_details TEXT NULL,

    # Indices and references
    reference_user_id INT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX users_id_unique_index (id ASC),
    UNIQUE INDEX users_email_unique_index (email ASC),
    UNIQUE INDEX users_phone_number_unique_index (phone_number ASC),
    CONSTRAINT fk_users_reference_user
        FOREIGN KEY (reference_user_id)
	    REFERENCES users (id)
	    ON DELETE SET NULL
	    ON UPDATE SET NULL);

CREATE TABLE users_roles (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # Indices and references
    user_id INT NOT NULL,
    role INT NOT NULL,

    PRIMARY KEY (id),
    UNIQUE INDEX users_roles_id_unique_index (id ASC),
    INDEX users_roles_user_id_index (user_id ASC),
    CONSTRAINT fk_users_roles_user
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE instructors (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # Indices and references
    user_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX instructors_id_unique_index (id ASC),
    UNIQUE INDEX instructors__user_id_unique_index (user_id ASC),
    CONSTRAINT fk_instructors_user
	    FOREIGN KEY (user_id)
	    REFERENCES users (id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE);

CREATE TABLE instructors_activities (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # Indices and references
    instructor_id INT NOT NULL,
    activity_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX instructors_activities_id_unique_index (id ASC),
    CONSTRAINT fk_instructors_activities_instructor
        FOREIGN KEY (instructor_id)
        REFERENCES instructors (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_instructors_activities_activity
        FOREIGN KEY (activity_id)
        REFERENCES activities (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE memberships (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # MembershipData fields
    membership_name VARCHAR(50) NOT NULL,
    price INT NOT NULL,
    currency INT NOT NULL,
    membership_level INT NOT NULL,
    max_sessions_number INT NULL,
    max_sessions_period INT NULL,

    # Indices and references
    PRIMARY KEY (id),
    UNIQUE INDEX memberships_id_unique_index (id ASC),
    UNIQUE INDEX memberships_membership_name_unique_index (membership_name ASC),
    UNIQUE INDEX memberships_membership_level_unique_index (membership_level ASC));

CREATE TABLE place_owners (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # Indices and references
    user_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX place_owners_id_unique_index (id ASC),
    UNIQUE INDEX place_owners_user_id_unique_index (user_id ASC),
    CONSTRAINT fk_place_owners_user
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE places (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # PlaceData fields
    place_name VARCHAR(50) NOT NULL,
    country VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL,
    address VARCHAR(50) NOT NULL,
    min_players INT NULL,
    max_players INT NOT NULL,
    latitude INT NOT NULL,
    longitude INT NOT NULL,

    # Indices and references
    place_owner_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX places_id_unique_index (id ASC),
    UNIQUE INDEX places_place_name_unique_index (place_name ASC),
    INDEX places_place_owner_id_index (place_owner_id ASC),
    CONSTRAINT fk_places_place_owner
        FOREIGN KEY (place_owner_id)
        REFERENCES place_owners (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE places_activities (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # PlaceActivityData fields
    min_players INT NULL,
    max_players INT NULL,

    # Indices and references
    place_id INT NOT NULL,
    activity_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX places_activities_id_unique_index (id ASC),
    INDEX places_activities_place_id_index (place_id ASC),
    INDEX places_activities_activity_id_index (activity_id ASC),
    CONSTRAINT fk_places_activities_place
        FOREIGN KEY (place_id)
        REFERENCES places (id)
        ON DELETE CASCADE
        ON UPDATE RESTRICT,
    CONSTRAINT fk_places_activities_activity
        FOREIGN KEY (activity_id)
        REFERENCES activities (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE players (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # Indices and references
    user_id INT NOT NULL,
    membership_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX players_id_unique_index (id ASC),
    UNIQUE INDEX players_user_id_unique_index (user_id ASC),
    INDEX players_membership_id_index (membership_id ASC),
    CONSTRAINT fk_players_user
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_players_membership
        FOREIGN KEY (membership_id)
        REFERENCES memberships (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE player_activity_levels (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # PlayerActivityData fields
    player_activity_level INT NOT NULL,

    # Indices and references
    player_id INT NOT NULL,
    activity_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX player_activity_levels_id_unique_index (id ASC),
    INDEX player_activity_levels_activity_id_index (activity_id ASC),
    INDEX player_activity_levels_player_id_index (player_id ASC),
    CONSTRAINT fk_player_activity_levels_activity
        FOREIGN KEY (activity_id)
        REFERENCES activities (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_player_activity_levels_player
        FOREIGN KEY (player_id)
        REFERENCES players (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE session_definitions (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # SessionDefinitionData fields
    title VARCHAR(50) NOT NULL,
    min_players INT NULL,
    max_players INT NULL,
    activity_level INT NULL,

    # Indices and references
    instructor_id INT NOT NULL,
    place_id INT NOT NULL,
    membership_id INT NOT NULL,
    activity_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX session_definitions_id_unique_index (id ASC),
    INDEX session_definitions_title_index (title ASC),
    INDEX session_definitions_instructor_id_index (instructor_id ASC),
    INDEX session_definitions_place_id_index (place_id ASC),
    INDEX session_definitions_membership_id_index (membership_id ASC),
    INDEX session_definitions_activity_id_index (activity_id ASC),
    CONSTRAINT fk_session_definitions_instructor
        FOREIGN KEY (instructor_id)
        REFERENCES instructors (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_session_definitions_place
        FOREIGN KEY (place_id)
        REFERENCES places (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_session_definitions_membership
        FOREIGN KEY (membership_id)
        REFERENCES memberships (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_session_definitions_activity
        FOREIGN KEY (activity_id)
        REFERENCES activities (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE session_definition_schedules (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # SessionDefinitionData fields
    day_of_week INT NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,

    # Indices and references
    session_definition_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX session_definition_schedules_id_unique_index (id ASC),
    INDEX session_definition_schedules_session_definition_index (session_definition_id ASC),
    CONSTRAINT fk_session_definition_schedules_session_definition
        FOREIGN KEY (session_definition_id)
        REFERENCES session_definitions (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE sessions (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # SessionData fields
    min_players INT NULL,
    max_players INT NULL,
    activity_level INT NULL,
    start_time DATETIME NULL,
    end_time DATETIME NULL,

    # Indices and references
    session_definition_id INT NOT NULL,
    instructor_id INT NOT NULL,
    place_id INT NOT NULL,
    membership_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX sessions_id_unique_index (id ASC),
    INDEX sessions_session_definition_id_index (session_definition_id ASC),
    INDEX sessions_instructor_id_index (instructor_id ASC),
    INDEX sessions_place_id_index (place_id ASC),
    INDEX sessions_membership_id_index (membership_id ASC),
    CONSTRAINT fk_sessions_session_definition
        FOREIGN KEY (session_definition_id)
        REFERENCES session_definitions (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_sessions_instructor
        FOREIGN KEY (instructor_id)
        REFERENCES instructors (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_sessions_place
        FOREIGN KEY (place_id)
        REFERENCES places (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_sessions_membership
        FOREIGN KEY (membership_id)
        REFERENCES memberships (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE players_sessions (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # PlayerSessionData fields
    player_arrived TINYINT NOT NULL,

    # Indices and references
    player_id INT NOT NULL,
    session_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX players_sessions_id_unique_index (id ASC),
    INDEX players_sessions_player_id_index (player_id ASC),
    INDEX players_sessions_session_id_index (session_id ASC),
    CONSTRAINT fk_players_sessions_player
        FOREIGN KEY (player_id)
        REFERENCES players (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_players_sessions_session
        FOREIGN KEY (session_id)
        REFERENCES sessions (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE staff_members (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # StaffMemberData fields
    permissions INT NOT NULL,

    # Indices and references
    user_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX staff_members_id_unique_index (id ASC),
    INDEX staff_members_user_idx (user_id ASC),
    CONSTRAINT fk_staff_members_user
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);

CREATE TABLE user_events (
    # BaseData fields
    id INT NOT NULL AUTO_INCREMENT,

    # BaseEntityData fields
    creation_time DATETIME NOT NULL,
    created_by VARCHAR(50) NOT NULL,
    last_modification_time DATETIME NOT NULL,
    last_modified_by VARCHAR(50) NOT NULL,
    active TINYINT NOT NULL,

    # UserEventData fields
    event_type INT NOT NULL,
    sub_event INT NULL,
    details TEXT NOT NULL,
    follow_up_time DATETIME NULL,

    # Indices and references
    user_id INT NOT NULL,
    staff_member_id INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX user_events_id_unique_index (id ASC),
    INDEX user_events_user_id_index (user_id ASC),
    INDEX user_events_staff_id_index (staff_member_id ASC),
    CONSTRAINT fk_user_events_user
        FOREIGN KEY (user_id)
        REFERENCES users (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_user_events_staff_member
        FOREIGN KEY (staff_member_id)
        REFERENCES staff_members (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE);